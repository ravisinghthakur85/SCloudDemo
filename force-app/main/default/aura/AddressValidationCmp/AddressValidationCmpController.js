({
    closeModel: function(component, event, helper) {
        component.set("v.isOpenAddressVal", false);
    },
    
    likenClose: function(component, event, helper) {
        // Display alert message on the click on the "Like and Close" button from Model Footer 
        // and set set the "isOpen" attribute to "False for close the model Box.
        component.set("v.isOpenAddressVal", false);
    },
    saveAddr : function(component, event, helper) {
        var action = component.get("c.saveAddress");
        var account = JSON.stringify(component.get("v.validatedAddress"));
        action.setParams({"avataxConfiguration": account});
        action.setCallback(this, function(response) { 
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.account',response.getReturnValue());
            }});
        $A.enqueueAction(action);
        component.set("v.isOpenAddressVal", false);
    },
})