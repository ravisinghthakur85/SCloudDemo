({
    cancelbnClick : function(component, event, helper) {
        window.location.href = location.origin;
    },
    getCredentialLst : function(component, event, helper) {
        var action = component.get('c.onConnect');
        var avaTaxConfiguration = component.get("v.avataxConfiguration");
        action.setParams({"avataxConfiguration": avaTaxConfiguration});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.isOpen", true);
                component.set('v.ListOfAvaTaxConfiguration', response.getReturnValue());
                component.set('v.companyOptions', response.getReturnValue().optionList);
                component.set("v.shippingOptions", response.getReturnValue().shippingCode);
            }
        });
        $A.enqueueAction(action);
    },
    
    displayCompanyCodeMapping : function(component, event, helper) {
        var customObjName= component.get("v.customObjName");
        var customFieldName = component.get("v.customFieldName");
        var page = component.get("v.page") || 1;
        
        var recordToDisply = "5";
        if($A.util.isUndefinedOrNull(customObjName) && $A.util.isUndefinedOrNull(customFieldName))
        {
            alert('Required field Missing! "Object Name for Subsidiary and Subsidiary Field Name on Account"');
        }
        else if($A.util.isUndefinedOrNull(customObjName) || customObjName.length == 0)
        {
            alert('Required field Missing! "Object Name for Subsidiary"');
        }
            else if($A.util.isUndefinedOrNull(customFieldName) || customFieldName.length == 0)
            {
                alert('Required field Missing! "Subsidiary Field Name on Account"');
            }
                else if(customObjName.length == 0 && customFieldName.length == 0 )
                {
                    alert('Required field Missing! "Object Name for Subsidiary" and "Subsidiary Field Name on Account"');
                }
        
                    else
                    {
                        // call the helper function   
                        helper.getAvaTaxConfiguration(component, page, recordToDisply,customObjName,customFieldName);
                    }    
    },
    
    getAddressLst : function(component, event, helper) {
        var action = component.get('c.addressValidation');
        var avaTaxConfiguration = component.get("v.avataxConfiguration");
        action.setParams({"avataxConfiguration": avaTaxConfiguration});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.isValidated',true);  
                if(response.getReturnValue().avError!=null)
                {
                    alert(response.getReturnValue().avError);    
                }
                else
                {               
                    component.set("v.isOpenAddressVal", true);
                    component.set('v.ListOfAddress',response.getReturnValue());
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    },
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
        //   window.location.href = location.href;
    },
    
    likenClose: function(component, event, helper) {
        component.set("v.isOpen", false);
        //    window.location.href = location.href;
        
    },
    
    init : function(component, event, helper) {
        component.set("v.baseUrl",location.origin);
        component.set("v.PageBlock", true);
        var customObjName;
        var customFieldName;
        var page = component.get("v.page") || 1;
        var recordToDisply = "5";
        helper.getAvaTaxConfiguration(component, page, recordToDisply,customObjName,customFieldName);
        helper.checkMetaDataAPIConnection(component,component.get("v.baseUrl"));
    }, 
    save : function(component, event, helper) {
        var action = component.get("c.saveAvataxConfiguration");
        var avaTaxConfiguration = component.get("v.avataxConfiguration");
        var conObj = component.get("v.objContact");
        var shippingCodeLocal = null;
        var companyCodeLocal = null;
        var multiCompanyMappingMap = component.get("v.multiCompanyMappingMap");
        var customObjName= component.get("v.customObjName");
        var customFieldName = component.get("v.customFieldName");
        
        if(component.get("v.shippingSelectedValue") != undefined){
            shippingCodeLocal = component.get("v.shippingSelectedValue");
        } 
        if(component.get("v.companySelectedValue") != undefined)
        {
            companyCodeLocal = component.get("v.companySelectedValue");
        }
        
        action.setParams({"avataxConfiguration": avaTaxConfiguration,
                          "shippingCode" : shippingCodeLocal,
                          "avaTaxCompanies" : JSON.stringify(component.get("v.companyOptions")),
                          "companyMapping":  JSON.stringify(multiCompanyMappingMap),
                          "companyCode":companyCodeLocal,
                          "customObjectName":customObjName,
                          "customFieldName":customFieldName
                         });
        action.setCallback(this, function(response) {  
            
            var state = response.getState();
            if (state === "SUCCESS") {
                if((response.getReturnValue().cautionString !="Configuration has been Saved!"))
                {
                    alert(response.getReturnValue().cautionString);
                }
                else
                {
                    component.set('v.avataxConfiguration',response.getReturnValue().avaTaxConfiguration);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    handleMappingUpdateEvent : function(component, event, helper) {
        var subdiaryNameEvent = event.getParam("subsidiaryName");
        var avataxCompanyCodeLocal = event.getParam("avataxCompanyCode");
        var mappingMap=component.get("v.multiCompanyMappingMap");
        
        if(mappingMap != '')
        {
            var isDuplicate = false;
            for(var i=0;i<mappingMap.length;i++)
            {
                if(mappingMap[i].subdiaryName == subdiaryNameEvent)
                {
                    isDuplicate =true;
                    break;
                }
            }
            if(isDuplicate){
                mappingMap[i].avataxCompanyCode = avataxCompanyCodeLocal;
            }
            else
            {
                mappingMap.push({"subdiaryName" : subdiaryNameEvent,"avataxCompanyCode" : avataxCompanyCodeLocal });
                
            }
        }
        else
        {
            mappingMap.push({"subdiaryName" : subdiaryNameEvent,"avataxCompanyCode" : avataxCompanyCodeLocal });
        }
        component.set("v.multiCompanyMappingMap",mappingMap);
    }, 
    
    navigate: function(component, event, helper) {
        var page = component.get("v.page") || 1;
        var direction = event.getSource().get("v.label");
        var recordToDisply = component.find("recordSize").get("v.value");
        page = direction === "Previous Page" ? (page - 1) : (page + 1);
        var customObjName;
        var customFieldName;
        
        helper.navigate(component, page, recordToDisply);
    },
    
    onSelectChange: function(component, event, helper) {
        var totalRecords = component.get("v.lstMultiCompanyMapping");
        var page = 1
        var recordToDisply = component.find("recordSize").get("v.value");
        var customObjName;
        var customFieldName;
        helper.navigate(component, page, recordToDisply);
    },
    
    getAccountList: function(component) {
        var action = component.get("c.getAccounts");
        var self = this;
        action.setCallback(this, function(actionResult) {
            component.set("v.accounts", actionResult.getReturnValue());            
        });
        $A.enqueueAction(action);
    },
    
    createRemoteSiteSetting : function(component, event, helper) {
        var binding = new XMLHttpRequest();
        
        
        var request =
            '<?xml version="1.0" encoding="utf-8"?>' +
            '<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+
            '<env:Header>' +
            '<urn:SessionHeader xmlns:urn="http://soap.sforce.com/2006/04/metadata">' +
            '<urn:sessionId>'+component.get('v.sessionId')+'</urn:sessionId>' +
            '</urn:SessionHeader>' +
            '</env:Header>' +
            '<env:Body>' +
            '<createMetadata xmlns="http://soap.sforce.com/2006/04/metadata">' +
            '<metadata xsi:type="RemoteSiteSetting">' +
            '<fullName>HostSystemRemoteSite</fullName>' +
            '<description>Metadata API Remote Site Setting for Declarative Rollup Tool (DLRS)</description>' +
            '<disableProtocolSecurity>false</disableProtocolSecurity>' +
            '<isActive>true</isActive>' +
            '<url>'+location.origin+'</url>' +
            '</metadata>' +
            '</createMetadata>' +
            '</env:Body>' +
            '</env:Envelope>';
        binding.open('POST', location.origin+'/services/Soap/m/31.0');
        binding.setRequestHeader('SOAPAction','""');
        binding.setRequestHeader('Content-Type', 'text/xml');
        binding.onreadystatechange =
            function() {
            if(this.readyState == 4) {
                var parser = new DOMParser();
                var doc  = parser.parseFromString(this.response, 'application/xml');
                var errors = doc.getElementsByTagName('errors');
                var messageText = '';
                for(var errorIdx = 0; errorIdx < errors.length; errorIdx++){
                    messageText+= errors.item(errorIdx).getElementsByTagName('message').item(0).innerHTML + '\n';
                }
                if(messageText.length == 0){
                    component.set('v.toDisplayCreateRemoteSiteButton',false);
                }
            }
        }
        binding.send(request);
    },
    
    mapperStudio : function(component, event, helper) {
        window.open(location.origin +'/apex/UIMapper','_blank');
    },
})