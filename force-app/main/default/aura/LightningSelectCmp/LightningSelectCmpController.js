({
    doInit: function(component, event, helper) {
        var multiCompanyMap = component.get("v.multiCompanyMap");
        var subdiaryName = component.get("v.subdiaryName");
        var cmpValue = component.get("v.listavataxConfiguration");
        var companyListing = cmpValue[0].companyListing;
        var avataxConfiguration= component.get("v.avaTaxConfiguration");
        var showTooltipLocal = 'false';
        
        for(var j = 0;j<multiCompanyMap.length;j++) {
            if(multiCompanyMap[j].subdiaryName == subdiaryName) {
                component.set("v.selectedValue",multiCompanyMap[j].avataxCompanyCode);
                var companyAddress ='';
                for(var i=0;i<companyListing.length;i++) {
                    if(companyListing[i].AVACLOUD_SAND__Avatax_Company_Code__c == multiCompanyMap[j].avataxCompanyCode)  {
                        if(companyListing[i].AVACLOUD_SAND__AddressLine1__c) {
                            companyAddress += companyListing[i].AVACLOUD_SAND__AddressLine1__c +','
                        }
                        if(companyListing[i].AVACLOUD_SAND__AddressLine2__c) {
                            companyAddress += companyListing[i].AVACLOUD_SAND__AddressLine2__c +','
                        }
                        if(companyListing[i].AVACLOUD_SAND__City__c) {
                            companyAddress += companyListing[i].AVACLOUD_SAND__City__c +','
                        }
                        if(companyListing[i].AVACLOUD_SAND__State__c) {
                            companyAddress += companyListing[i].AVACLOUD_SAND__State__c +','
                        }
                        if(companyListing[i].AVACLOUD_SAND__PostalCode__c) {
                            companyAddress += companyListing[i].AVACLOUD_SAND__PostalCode__c +','
                        }
                        if(companyListing[i].AVACLOUD_SAND__Country__c) {
                            companyAddress += companyListing[i].AVACLOUD_SAND__Country__c +','
                        }
                        
                        if(!companyAddress) {
                            showTooltipLocal = 'true';
                            if(avataxConfiguration.AVACLOUD_SAND__Street__c) {
                                companyAddress += avataxConfiguration.AVACLOUD_SAND__Street__c	 +','
                            }
                            if(avataxConfiguration.AVACLOUD_SAND__Line2__c) {
                                companyAddress += avataxConfiguration.AVACLOUD_SAND__Line2__c +','
                            }
                            if(avataxConfiguration.AVACLOUD_SAND__City__c) {
                                companyAddress += avataxConfiguration.AVACLOUD_SAND__City__c +','
                            }
                            if(avataxConfiguration.AVACLOUD_SAND__State__c) {
                                companyAddress += avataxConfiguration.AVACLOUD_SAND__State__c +','
                            }
                            if(avataxConfiguration.AVACLOUD_SAND__Postal_Code__c) {
                                companyAddress += avataxConfiguration.AVACLOUD_SAND__Postal_Code__c +','
                            }
                            if(avataxConfiguration.AVACLOUD_SAND__Country__c) {
                                companyAddress += avataxConfiguration.AVACLOUD_SAND__Country__c 
                            }
                        }
                        break;
                    }
                } 
            }
        }
    },
    
    updateCompany: function(component, event, helper) {
        var companySelectedValue= component.get("v.selectedValue");
        var cmpValue = component.get("v.listavataxConfiguration");
        var companyListing = cmpValue[0].companyListing;
        var showTooltipLocal = false;
        var companyAddress ='';
        var avataxConfiguration= component.get("v.avaTaxConfiguration");
        var account= component.get("v.account");
        
        
        for(var i=0;i<companyListing.length;i++) {
            if(companyListing[i].AVACLOUD_SAND__Avatax_Company_Code__c == companySelectedValue) {
                if(companyListing[i].AVACLOUD_SAND__AddressLine1__c ) {
                    companyAddress += companyListing[i].AVACLOUD_SAND__AddressLine1__c +','
                }
                if(companyListing[i].AVACLOUD_SAND__AddressLine2__c ) {
                    companyAddress += companyListing[i].AVACLOUD_SAND__AddressLine2__c +','
                }
                if(companyListing[i].AVACLOUD_SAND__City__c) {
                    companyAddress += companyListing[i].AVACLOUD_SAND__City__c +','
                }
                if(companyListing[i].AVACLOUD_SAND__State__c) {
                    companyAddress += companyListing[i].AVACLOUD_SAND__State__c +','
                }
                if(companyListing[i].AVACLOUD_SAND__PostalCode__c) {
                    companyAddress += companyListing[i].AVACLOUD_SAND__PostalCode__c +','
                }
                if(companyListing[i].AVACLOUD_SAND__Country__c) {
                    companyAddress += companyListing[i].AVACLOUD_SAND__Country__c ;
                }
                
                if(!companyAddress) {
                    if(avataxConfiguration.AVACLOUD_SAND__Street__c) {
                        companyAddress += avataxConfiguration.AVACLOUD_SAND__Street__c +',';
                    }
                    if(avataxConfiguration.AVACLOUD_SAND__Line2__c) {
                        companyAddress += avataxConfiguration.AVACLOUD_SAND__Line2__c +',';
                    }
                    if(avataxConfiguration.AVACLOUD_SAND__City__c) {
                        companyAddress += avataxConfiguration.AVACLOUD_SAND__City__c +',';
                    }
                    if(avataxConfiguration.AVACLOUD_SAND__State__c)  {
                        companyAddress += avataxConfiguration.AVACLOUD_SAND__State__c +',';
                    }
                    if(avataxConfiguration.AVACLOUD_SAND__Postal_Code__c) {
                        companyAddress += avataxConfiguration.AVACLOUD_SAND__Postal_Code__c +',';
                    }
                    if(avataxConfiguration.AVACLOUD_SAND__Country__c) {
                        companyAddress += avataxConfiguration.AVACLOUD_SAND__Country__c;
                    }
                    showTooltipLocal = true;
                }
                break;
            }
            
        }
        var subdiaryName = component.get("v.subdiaryName");
        
        var mappingevent = $A.get("e.c:MultiMappingRecordEvt");
        mappingevent.setParams({
            "showTooltip" :showTooltipLocal,
            "avataxCompanyCode" : companySelectedValue,
            "subsidiaryName" : subdiaryName
        });
        mappingevent.fire();
        
        var appEvent = $A.get("e.c:AvaCompanyAddressUpdateEvt");
        appEvent.setParams({
            "showTooltip" :showTooltipLocal,
            "companyAddress" : companyAddress,
            "subdiaryName" : subdiaryName
        });    
        appEvent.fire();
        
    },
})