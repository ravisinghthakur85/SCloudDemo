public class OrderTaxCalculatorHelper implements AVA_MAPPER.ICustomBusinessCase  
{
    string accountId = '';
    string accountName = '';
    string accountNumber ='';
    string orderId = '';
    string subsidiaryFieldAPINameLocal = '';
    
    Boolean commitFlag = false;
    
    public OrderTaxCalculatorHelper(boolean commitStatus,string subsidiaryFieldAPIName)	{
        System.debug('In constructor');
            
        commitFlag = commitStatus;
        subsidiaryFieldAPINameLocal = subsidiaryFieldAPIName;
    }
    
    public void execute(AVA_MAPPER.CreateTransactionModel model, Map <String, List <SObject>> headerResults, Map <String, List <SObject >> lineQueryResult) 
    {
        System.debug('In execute');
        try
        {
            if (headerResults.containsKey('order')) 
            {
                List <Order> order = headerResults.get('order');
                
                for (Order orderData: order)
                {
                    model.Type = (AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Save_transactions_to_AvaTax__c) ? AVA_Mapper.DocumentType.SalesInvoice : AVA_Mapper.DocumentType.SalesOrder;
                    
                    if(model.addresses == null)
                    {
                        model.addresses = new AVA_Mapper.AddressesModel();
                    }

                    System.debug('model.addresses.shipFrom before loop: '+model.addresses.shipFrom);
                    if (String.isNotBlank(subsidiaryFieldAPINameLocal) && (subsidiaryFieldAPINameLocal.indexOf('null') == -1) )
                    {
                        string subsidiaryId = string.valueOf(UtilityHelper.getFieldValue(orderData,subsidiaryFieldAPINameLocal));
                        System.debug('subsidiaryId: '+subsidiaryId);
                        UtilityHelper.setShipFromAddressAndCompany(subsidiaryId,model);
                    }
                    System.debug('model.addresses.shipFrom after loop: '+model.addresses.shipFrom);

                    if(orderData.Account.AVA_MAPPER__Business_Identification_Number__c != null)
                    {
                        model.businessIdentificationNo = orderData.Account.AVA_MAPPER__Business_Identification_Number__c; 
                    }
                    AVA_MAPPER__Entity_Use_code__c entityuserec = UtilityHelper.fetchEntityUseCode(orderData.Entity_Use_Code__c);
                    model.customerUsageType = UtilityHelper.trimData(entityuserec.Name, AvaConstants.ENTITY_USE_CODE_LEN);
                    
                    orderId = orderData.Id;
                    accountId = orderData.Account.Id;
                    accountNumber = orderData.Account.AccountNumber;
                    accountName = UtilityHelper.trimData(orderData.Account.Name, AvaConstants.ACCOUNT_NAME);
                    
                    List<MultiCompanyMapping__c> multiCompanyMappingData = new List<MultiCompanyMapping__c>();

                    //Once the document is committed, we're not changing status for it
                    if(orderData.AVACLOUD_SAND__AvaTax_Doc_Status__c ==  'Committed')
                    {
                        model.commitDocument = True;
                    }
                    else
                    {
                        model.commitDocument = commitFlag;
                    }
                    
                    model.customerCode = (accountNumber != null) ? String.valueOf(accountNumber) : accountName;
                    
					AVA_MAPPER.AddressLocationInfo shipToAddress = new AVA_MAPPER.AddressLocationInfo();
                    //To check fallback address
                    if(orderData.ShippingStreet == null && orderData.ShippingCity == null && orderData.ShippingState == null && orderData.ShippingPostalCode == null 
                       && orderData.ShippingCountry == null)
                    {
                        shiptoAddress.line1 = orderData.BillingStreet;
                        shiptoAddress.city = orderData.BillingCity;
                        shiptoAddress.region = orderData.BillingState;
                        shiptoAddress.country = orderData.BillingCountry;
                        shiptoAddress.postalcode = orderData.BillingPostalCode;
                    }
                    
                    if(model.addresses.shipTo == null && model.addresses.singleLocation == null && model.addresses.pointOfOrderAcceptance == null 
                       && model.addresses.pointOfOrderOrigin == null)
                    {
                    	model.addresses.shipTo = shipToAddress;
                    }
                    
                    if(model.addresses.shipTo != null && model.addresses.shipTo.city == null && model.addresses.shipTo.country == null
                       && model.addresses.shipTo.line1 == null && model.addresses.shipTo.line2 == null && model.addresses.shipTo.line3 == null
                       && model.addresses.shipTo.locationCode == null && model.addresses.shipTo.postalCode == null && model.addresses.shipTo.region == null)
                    {
                    	model.addresses.shipTo = shipToAddress;
                    }

                    if(orderData.Tax_Date__c != null && orderData.tax_date__c != orderData.CreatedDate)
                    {
                        AVA_MAPPER.TaxOverrideModel taxOverride = new AVA_MAPPER.TaxOverrideModel();
                        taxOverride.Reason = AvaConstants.TAX_OVERRIDE_REASON_DATE;
                        taxOverride.type = AVA_MAPPER.TaxOverrideType.TaxDate;  //TaxDate override
                        taxOverride.TaxDate = orderData.Tax_Date__c;
                        model.taxOverride = taxOverride;
                    }
                    
                    //Added for VAT implementation
                    model.currencyCode = UserInfo.getDefaultCurrency();
                    if(UserInfo.IsMultiCurrencyOrganization()){
                        if(NULL != Schema.SObjectType.Order.fields.GetMap().Get('CurrencyIsoCode')){
                            model.currencyCode = String.valueof(orderData.Get('CurrencyIsoCode'));
                        }
                    }
                }
            }
            
            if (lineQueryResult.containsKey('orderitem')) 
            {
                List <OrderItem> orderLine = lineQueryResult.get('orderitem');
                Integer orderLineSize = orderLine.size();
                
                for (Integer lineCnt=0; lineCnt < orderLineSize; lineCnt++)
                {
                    model.Lines[lineCnt].Description = UtilityHelper.trimData(orderLine[lineCnt].Pricebookentry.product2.Description, AvaConstants.PRODUCT_DESCRPTION_LEN);
                    if(orderLine[lineCnt].Pricebookentry.ProductCode == null)
                    {
                        model.Lines[lineCnt].ItemCode = UtilityHelper.trimData(orderLine[lineCnt].Pricebookentry.product2.Name, AvaConstants.ITEM_CODE);
                    }
                    else
                    {
                        model.Lines[lineCnt].ItemCode = UtilityHelper.trimData(orderLine[lineCnt].Pricebookentry.ProductCode, AvaConstants.ITEM_CODE);
                    }
                    
                    if(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Enable_UPC_Code_as_ItemCode__c && orderLine[lineCnt].Pricebookentry.product2.UPC__c != null)
                    {
                        model.Lines[lineCnt].ItemCode = UtilityHelper.trimData(('UPC:'+orderLine[lineCnt].Pricebookentry.product2.UPC__c), AvaConstants.ITEM_CODE);
                    }

                    if(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Allow_Tax_Override__c == true && orderLine[lineCnt].Tax_Override__c == true 
                       && orderLine[lineCnt].Sales_Tax_Amount_Line__c != null)
                    {
                        AVA_MAPPER.TaxOverrideModel taxOverrideAmount = new AVA_MAPPER.TaxOverrideModel();
                        taxOverrideAmount.Reason = AvaConstants.TAX_OVERRIDE_REASON_AMOUNT;
                        taxOverrideAmount.type = AVA_MAPPER.TaxOverrideType.TaxAmount;  //TaxDate override
                        taxOverrideAmount.TaxAmount = orderLine[lineCnt].Sales_Tax_Amount_Line__c;
                        model.Lines[lineCnt].taxOverride = taxOverrideAmount;
                    }
                }
            }
        }
        catch (Exception e) 
        {
            throw new SalesCloudAvaTaxException('Cause '+e.getCause() +' , '+ 'Message '+e.getMessage() +' , '+ 'Stack Trace '+ e.getStackTraceString(),e);
        }
    }
}