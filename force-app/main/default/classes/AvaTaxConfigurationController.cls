/*Controller for AvaTax Configuration DML and Configuration Address Validation */
public with sharing class AvaTaxConfigurationController {
    
    static string logs;
    
    // AuraEnabled function for Test Connection
    @AuraEnabled
    public static AvaTaxTestConnectionResponse onConnect(AvaTax__c avataxConfiguration)
    {
        AvaTaxTestConnectionResponse returnString = new AvaTaxTestConnectionResponse();
        
        try
        { 
            ConfigurationProvider cp = new ConfigurationProvider();
            AvaTaxConfigManager am = new AvaTaxConfigManager();
            am = getavaTaxConfig(avataxConfiguration);
            if((string.isBlank(am.username) || string.isBlank(am.password)))
            {
                returnString.tcResponse = AvaConstants.INVALID_CREDENTIAL_AVATAX;
                return returnString;
            }
            
            AVA_MAPPER.AvaTaxClient client = AVA_MAPPER.AvaTaxUtilities.getClient(am);
            AVA_MAPPER.FetchResultSubscriptionModel result = new AVA_MAPPER.FetchResultSubscriptionModel(); 
            if(!Test.isRunningTest())
            {
                result = client.GetMySubscription();
            }
            else
            {
                result.statusCode = 200;
            }
            if(result.statusCode == 200 || result.statusCode == 201)
            {
                returnString.tcResponse = AvaConstants.CONNECTED_CREDENTIAL_AVATAX;
                List<AVACLOUD_SAND__AvaTaxCompany__c> listCompanyCode = companyCodeList(avataxConfiguration);
                returnString.optionList = listCompanyCode;
                
                List<AVA_MAPPER__Entity_Use_Code__c> entitUseCodeResult = entityUseCodeList(avataxConfiguration);
                
                List<AVACLOUD_SAND__Shipping_Code__c> shippingCodeResult = shippingCodeList(avataxConfiguration);
                returnString.shippingCode = shippingCodeResult;
                
                logs = UtilityHelper.createTestConnectionLogs('ConfigAudit','ConfigurationPage', 'TestConnection','Informational',avataxConfiguration,'onConnect',null);
                system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));
                
                if(entitUseCodeResult.size() > 0)
                {
                    AVA_MAPPER.AvaTaxUtilities.updateEntityUseCodes(entitUseCodeResult);
                }
                
                if(shippingCodeResult.size() > 0)
                {
                    UtilityHelper.updateFreightCodes(shippingCodeResult);
                }
                
                if(listCompanyCode.size() > 0)
                {
                    List<AVACLOUD_SAND__AvaTaxCompany__c> ac = getCompanyList();
                    
                    if(Schema.sObjectType.AVACLOUD_SAND__AvaTaxCompany__c.isDeletable())
                    {
                    	delete ac;
                    }

                    if(Schema.sObjectType.AVACLOUD_SAND__AvaTaxCompany__c.isCreateable())
                    {
                    	insert listCompanyCode;
                    }
                }
            }
            else if(result.statusCode == 501)
            {
                logs = UtilityHelper.createTestConnectionLogs('ConfigAudit','ConfigurationPage', 'TestConnection','Informational',null,'onConnect',result.error.message);
                logs = UtilityHelper.trimNewLineCharacterFromString(logs);
                system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));
            }
            else
            {
                returnString.tcResponse = result.error.message;
                avataxConfiguration.AVACLOUD_SAND__Company_Code__c = '';
                returnString.configurationList = avataxConfiguration;
            }
        }
        catch(Exception e)
        {
            logs = UtilityHelper.createTestConnectionLogs('ConfigAudit','ConfigurationPage', 'TestConnection','Informational',null,'onConnect',string.valueOf(e.getMessage()+','+e.getStackTraceString()));
            system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));
            throw new AVA_MAPPER.AvaTaxException(e.getMessage()+','+e.getStackTraceString());
        }
        return returnString;        
    }
    
    // AuraEnabled function for retrieving configuration from Salesforce
    @AuraEnabled public static AvaTaxTestConnectionResponse getAvataxConfiguration(){
        try
        {
            AvaTaxTestConnectionResponse returnString = new AvaTaxTestConnectionResponse();
            AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
            returnString.configurationList = avaTaxInstance;
            List<Shipping_Code__c> shippingCodeList = [select id,Name from Shipping_Code__c order by name asc Limit 10000];
            if(shippingCodeList!=null & shippingCodeList.size() > 0)
            {
                returnString.shippingCode =shippingCodeList;
            }
            List<AvaTaxCompany__c> companyList = getCompanyList();
            returnString.optionList = companyList;
            for(AvaTaxCompany__c company : companyList)
            {
                if(company.Avatax_Company_Code__c == avaTaxInstance.Company_Code__c)
                {
                    returnString.defaultValue =company.Avatax_Company_Code__c;
                }
            }
            return  returnString;
        }
        catch(Exception e)
        {
            throw new AVA_MAPPER.AvaTaxException(e.getMessage()+','+e.getStackTraceString());
        }
    }
    
    public static List<MultiCompanyModel> getMultiCompanyMapping(String customObjName,String customFieldName)
    {
        List<MultiCompanyModel> multiCompanyList = new List<MultiCompanyModel>();
        AdditionalSettings__c cmm = AdditionalSettings__c.getInstance('AvaTax');
        
        try
        {
            List<MultiCompanyMapping__c> multiCompanyDefault = new List<MultiCompanyMapping__c>();
            List<AvaTaxCompany__c> companyDetailList = new List<AvaTaxCompany__c>();
            if (Schema.sObjectType.AvaTaxCompany__c.fields.Name.isUpdateable()) {
                
                companyDetailList = [Select Avatax_Company_Code__c, AddressLine1__c, AddressLine2__c, City__c, State__c, PostalCode__c, Country__c 
                                     from AvaTaxCompany__c ORDER BY Avatax_Company_Code__c LIMIT 1000];
            }
            List<MultiCompanyMapping__c> multiCompany = new  List<MultiCompanyMapping__c>();
            if (Schema.sObjectType.MultiCompanyMapping__c.fields.Name.isUpdateable()) {
                
                multiCompany = [Select SFCompanyId__c,SubsidiaryName__c, AvaCompanyCode__c from MultiCompanyMapping__c LIMIT 50000];
            }
            if (cmm.CustomObjectApiName__c != null && cmm.CustomObjectApiName__c.length() != 0)
            {
                String objName = cmm.CustomObjectApiName__c;
                String query = string.escapeSingleQuotes('Select Id,Name from ' + cmm.CustomObjectApiName__c + '   order by Name');// limit :recordToDisply offset :offset';//+' ORDER BY Name ASC NULLS LAST LIMIT :blockSize OFFSET :index';// + ' Limit 15';
                List<SObject> objL = Database.query(query);
                
                for (Integer i = 0; i < objL.size(); i++)
                {
                    MultiCompanyModel mcm = new MultiCompanyModel();
                    mcm.sfCompany = String.ValueOf(objL[i].get('Name'));
                    mcm.sfCompanyId = String.ValueOf(objL[i].get('Id'));
                    mcm.companyListing = companyDetailList;
                    
                    for (Integer j = 1; j <= multiCompany.size(); j++)
                    {
                        if (multiCompany[j - 1].SubsidiaryName__c == mcm.sfCompanyId)
                        {
                            mcm.avaCompanyCode = multiCompany[j - 1].AvaCompanyCode__c;
                            mcm.companyAddress ='';
                            for (AvaTaxCompany__c company : companyDetailList)
                            {
                                if (company.Avatax_Company_Code__c == multiCompany[j - 1].AvaCompanyCode__c)
                                {
                                    if (company.AddressLine1__c != null
                                        || company.AddressLine2__c != null || company.City__c != null || company.Country__c != null
                                        || company.PostalCode__c != null || company.State__c != null)
                                    {
                                        //Check every field for null and if null then make blank                           
                                        mcm.companyAddress = company.AddressLine1__c == null ?'' :company.AddressLine1__c + ','
                                            +(company.AddressLine2__c == null ?'' :company.AddressLine2__c + ',')
                                            +(company.City__c == null ?'' :company.City__c + ',')
                                            +(company.State__c == null ?'' :company.State__c + ',')
                                            +(company.PostalCode__c == null ?'' :company.PostalCode__c + ',')
                                            +(company.Country__c == null ?'' :company.Country__c           );
                                    }
                                    else
                                    {
                                        mcm.toolTipWarning = 'Please update the company address in AvaTax Companies Tab to use it as a “Ship From” address for tax calculation';
                                        mcm.warningSignRender = true;
                                        AVA_MAPPER.AddressLocationInfo subsidiaryCompanyAddress = new AVA_MAPPER.AddressLocationInfo();
                                        subsidiaryCompanyAddress = UtilityHelper.generateOriginAddress();
                                        
                                        if(string.isNotBlank(subsidiaryCompanyAddress.line1))
                                        {
                                            mcm.companyAddress += subsidiaryCompanyAddress.line1 + ',';
                                        }
                                        if(string.isNotBlank(subsidiaryCompanyAddress.line2))
                                        {
                                            mcm.companyAddress += subsidiaryCompanyAddress.line2 + ',';
                                        }
                                        if(string.isNotBlank(subsidiaryCompanyAddress.line3))
                                        {
                                            mcm.companyAddress += subsidiaryCompanyAddress.line3 + ',';
                                        }
                                        if(string.isNotBlank(subsidiaryCompanyAddress.city))
                                        {
                                            mcm.companyAddress += subsidiaryCompanyAddress.city + ',';
                                        }
                                        if(string.isNotBlank(subsidiaryCompanyAddress.region))
                                        {
                                            mcm.companyAddress += subsidiaryCompanyAddress.region + ',';
                                        }
                                        if(string.isNotBlank(subsidiaryCompanyAddress.postalcode))
                                        {
                                            mcm.companyAddress += subsidiaryCompanyAddress.postalcode + ',';
                                        }
                                        if(string.isNotBlank(subsidiaryCompanyAddress.country))
                                        {
                                            mcm.companyAddress += subsidiaryCompanyAddress.country ;
                                        }
                                        
                                    }
                                }
                            }
                            break;
                        }
                    }
                    multiCompanyList.add(mcm);
                }
                multiCompanyList.sort();
                
            }
            system.debug('multiCompanyList'+multiCompanyList);
            return multiCompanyList; 
        }
        catch(Exception e)
        {
            throw new AVA_MAPPER.AvaTaxException(e.getMessage()+','+e.getStackTraceString());
        }
    }
    
    
    @AuraEnabled public static AvaTaxTestConnectionResponse getAvaTaxMultiEntityConfiguration(string customObjName,string customFieldName)
    {       
        //Added for multi-entity 
        AvaTaxTestConnectionResponse returnString =  new AvaTaxTestConnectionResponse(); 
        
        try
        {
            returnString.displayMultiMapping = false;
            // create a instance of wrapper class.
            AVACLOUD_SAND__AdditionalSettings__c cmm = AVACLOUD_SAND__AdditionalSettings__c.getInstance('AvaTax');
            system.debug('cmm'+cmm);
            
            if(customObjName != null)
            {
                Integer recordCount = Database.countQuery(string.escapeSingleQuotes('Select Count() from ' + customObjName));
                returnString.total = recordCount;
            }
            else  if(cmm.CustomObjectApiName__c != null)
            {
                Integer recordCount = Database.countQuery(string.escapeSingleQuotes('Select Count() from ' + cmm.CustomObjectApiName__c));
                returnString.total = recordCount;
            }
            if (customFieldName != null)
            {
                if (customFieldName.length() != 0)
                {
                    List<Account> acc = new List<Account>();
                    string accQuery =string.escapeSingleQuotes('Select ' + customFieldName + ' from Account Limit 1');
                    List<SObject> objAcc = Database.query(accQuery);
                }
            }
            
            if(customObjName != null && customFieldName != null)
            {
                System.debug('In getAvaTaxMultiEntityConfiguration if');
                upsertAddiionalSettings(customObjName, customFieldName);               
                returnString.customObjName = customObjName;
                returnString.customFieldName = customFieldName;
                returnString.displayMultiMapping = true;
            }
            else
            {
                System.debug('In getAvaTaxMultiEntityConfiguration else');
                returnString.customObjName = cmm.CustomObjectApiName__c;
                returnString.customFieldName = cmm.CustomFieldApiName__c;
                if(cmm.CustomObjectApiName__c != null && cmm.CustomFieldApiName__c != null)
                {
                    returnString.displayMultiMapping = true;
                }
            }
            
            //Added for multi-entity code. Check missing vlaue 
            returnString.avaTaxConfiguration = AvaTax__c.getInstance('AvaTax Config');
            
            List<MultiCompanyModel> multiCompanyList = getMultiCompanyMapping(customObjName, customFieldName);
            //obj.countryListing = listcountryCode;
            if (Schema.sObjectType.AvaTaxCompany__c.fields.Name.isUpdateable()) {
                
                returnString.optionList = [Select Avatax_Company_Code__c from AvaTaxCompany__c LIMIT 1000 ];
            }
            
            if (Schema.sObjectType.AVACLOUD_SAND__Shipping_Code__c.fields.Name.isUpdateable()) {
                returnString.shippingCode = [Select Name,Description__c from AVACLOUD_SAND__Shipping_Code__c ORDER BY Name LIMIT 1000 ];
            }
            
            returnString.lstMultiCompanyMapping = multiCompanyList;
            system.debug('returnString.companyListing '+multiCompanyList );
        }
        catch(Exception e)
        {
            returnString.cautionString = e.getMessage()+','+e.getStackTraceString();
            return returnString;
        }
        return returnString;
    }
    
    // AuraEnabled function for retrieving AvaTax Company Codes from Salesforce 
    @AuraEnabled 
    public static List<AvaTaxCompany__c> getCompanyList()
    {
        list<AvaTaxCompany__c> companyList = [select Avatax_Company_Code__c,Name from AvaTaxCompany__c limit 50000];
        return companyList;
    }
    
    @AuraEnabled
    public Static void upsertAddiionalSettings(string customObjName,string customFieldName)
    {
        try
        {
            AVACLOUD_SAND__AdditionalSettings__c config = AVACLOUD_SAND__AdditionalSettings__c.getInstance('AvaTax');
            config.CustomFieldApiName__c=customFieldName;
            config.CustomObjectApiName__c=customObjName; 
            
            if (Schema.sObjectType.AVACLOUD_SAND__AdditionalSettings__c.fields.Name.isUpdateable() && Schema.sObjectType.AVACLOUD_SAND__AdditionalSettings__c.fields.Name.isCreateable()){
                upsert config;
            }
        }
        catch (Exception e)
        {
            throw new AVA_MAPPER.AvaTaxException('Exception - '+e.getMessage());
        }
    }
    
    
    // AuraEnabled function for Saving AvaTax Configuration in Salesforce
    @AuraEnabled public static AvaTaxTestConnectionResponse saveAvataxConfiguration(AvaTax__c avataxConfiguration,string shippingCode,String avaTaxCompanies,String companyMapping,string companyCode,string customObjectName, string customFieldName){
        string message;
        AvaTaxTestConnectionResponse accPage = new AvaTaxTestConnectionResponse();
        
        try{
            if(string.isNotBlank(customObjectName) && string.isNotBlank(customFieldName))
            {
                fetchList(customObjectName, customFieldName);
            }
            List<MultiCompanyMapping__c> mcmList = new List<MultiCompanyMapping__c>();
            List<MultiCompanyMapping__c> multiCompanyDBMapping = new  List<MultiCompanyMapping__c>();
            
            if (Schema.sObjectType.MultiCompanyMapping__c.fields.Name.isUpdateable()) {
                multiCompanyDBMapping = [Select id, AvaCompanyCode__c,SubsidiaryName__c from MultiCompanyMapping__c LIMIT 1000];
            }
            
            System.debug('companyMapping: '+companyMapping);
            List<CompanyMappingModel> responseList = (List<CompanyMappingModel>)JSON.deserialize(companyMapping, List<CompanyMappingModel>.class);
            
            System.debug('responseList: '+responseList);
            System.debug('multiCompanyDBMapping: '+multiCompanyDBMapping);
            
            for(CompanyMappingModel cmm : responseList)
            {   
                MultiCompanyMapping__c mcm = new MultiCompanyMapping__c();
                if(multiCompanyDBMapping.size() !=0)
                {   
                    for(MultiCompanyMapping__c mmc : multiCompanyDBMapping)
                    {
                        if(mmc.SubsidiaryName__c == cmm.subdiaryName)
                        {
                            mcm.Id = mmc.Id;
                            mcm.AvaCompanyCode__c = cmm.avataxCompanyCode;
                            mcm.SubsidiaryName__c = cmm.subdiaryName;
                            System.debug('In 1');
                            break;
                        }
                        else
                        { 
                            mcm.AvaCompanyCode__c = cmm.avataxCompanyCode;
                            mcm.SubsidiaryName__c = cmm.subdiaryName;
                            System.debug('In 2');
                        }
                    }
                }
                else
                { 
                    mcm.AvaCompanyCode__c = cmm.avataxCompanyCode;
                    mcm.SubsidiaryName__c = cmm.subdiaryName;
                    System.debug('In 3');
                }   
                if(mcm.AvaCompanyCode__c != null && mcm.SubsidiaryName__c != null)
                {
                    System.debug('mcm in if: '+mcm);
                    mcmList.add(mcm);
                }
                else
                {
                    System.debug('mcm in else: '+mcm);
                }
            }
            if (Schema.sObjectType.MultiCompanyMapping__c.fields.Name.isUpdateable() && Schema.sObjectType.MultiCompanyMapping__c.fields.Name.isCreateable()){
                System.debug('mcmList in update: '+mcmList);
                upsert mcmList;
            }
            
            if((string.isBlank(string.valueOf(avataxConfiguration.AVACLOUD_SAND__Account_ID__c)) || string.isBlank(avataxConfiguration.AVACLOUD_SAND__License_Key__c) || string.isBlank(companyCode)))
            {
                //message = 'Required Fields are missing on Account Credentials section';
                accPage.cautionString = 'Required Fields are missing on Account Credentials section';
            }
            else
            {
                avataxConfiguration.AVACLOUD_SAND__Shipping_Code__c = shippingCode;
                avataxConfiguration.AVACLOUD_SAND__Company_Code__c = companyCode;
                
                Database.UpsertResult theResult = Database.upsert(avataxConfiguration, false );
                system.debug('theResult'+theResult);
                if(theResult.isSuccess())
                {
                    logs = UtilityHelper.createTestConnectionLogs('ConfigAudit','ConfigurationPage', 'ConfigChanges','Informational',avataxConfiguration,'saveAvataxConfiguration',null);
                    system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));
                    message = AvaConstants.CONFIGURATION_CREDENTIAL_SAVE;
                    
                    accPage.avaTaxConfiguration = avataxConfiguration;
                    message = AvaConstants.CONFIGURATION_CREDENTIAL_SAVE;
                    accPage.cautionString = message;                    
                }
                else
                {
                    List<Database.Error> errors = theResult.getErrors();
                    for(Database.Error theError:Errors) {
                        accPage.cautionString = theError.getMessage();                        
                    }
                }
            }
        }
        catch(Exception e)
        {
            accPage.cautionString = UtilityHelper.trimNewLineCharacterFromString(e.getMessage());
            logs = UtilityHelper.createTestConnectionLogs('ConfigAudit','ConfigurationPage', 'ConfigChanges','Informational',null,'saveAvataxConfiguration',string.valueOf(e.getMessage()+','+e.getStackTraceString()));
            system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));
            //throw new AVA_MAPPER.AvaTaxException(e.getMessage()+','+e.getStackTraceString());
        }        
        //return message;
        return accPage;
    } 
    
    
    @AuraEnabled
    public Static void fetchList(string customObjName,string customFieldName)
    {
        try
        {
            
            AVACLOUD_SAND__AdditionalSettings__c config = AVACLOUD_SAND__AdditionalSettings__c.getInstance('AvaTax');
            config.CustomFieldApiName__c=customFieldName;
            config.CustomObjectApiName__c=customObjName;
            string accQuery =string.escapeSingleQuotes('Select ' + customFieldName + ' from Account Limit 1');
            List<SObject> objAcc = Database.query(accQuery);
            
            if (Schema.sObjectType.AVACLOUD_SAND__AdditionalSettings__c.fields.Name.isUpdateable() && Schema.sObjectType.AVACLOUD_SAND__AdditionalSettings__c.fields.Name.isCreateable()){
                upsert config;
            }
        }
        catch (Exception e)
        {
            throw new AVA_MAPPER.AvaTaxException('Exception - '+e.getMessage());
        }
    }
    
    // AuraEnabled function for retrieving Credentials from avataxConfiguration object from Lightning
    public static AvaTaxConfigManager getavaTaxConfig(AvaTax__c avataxConfiguration)
    {
        AvaTaxConfigManager config = new AvaTaxConfigManager();
        config.username = string.isNotBlank(avataxConfiguration.AVACLOUD_SAND__Account_ID__c) ? avataxConfiguration.AVACLOUD_SAND__Account_ID__c : null;
        config.password =string.isNotBlank(avataxConfiguration.AVACLOUD_SAND__License_Key__c) ? avataxConfiguration.AVACLOUD_SAND__License_Key__c : null;
        config.environment = AVA_MAPPER.AvaTaxEnvironment.Production;
        
        if(avataxConfiguration.AVACLOUD_SAND__Sandbox__c == True) 
        {
            config.environment = AVA_MAPPER.AvaTaxEnvironment.Sandbox;
        }
        return config;
    }
    
    // AuraEnabled function for Address Validation
    @AuraEnabled
    public static AddressValidationParams addressValidation(AvaTax__c avataxConfiguration)
    {
        try
        {
            AddressValidationParams val = new AddressValidationParams();
            ConfigurationProvider sf = new ConfigurationProvider();
            AvaTaxConfigManager configuration = new AvaTaxConfigManager();
            configuration = sf.getavaTaxConfig();
            AVA_MAPPER.AvaTaxClient client = AVA_MAPPER.AvaTaxUtilities.getClient(configuration);
            AVA_MAPPER.AddressResolutionModel result = null;
            if(configuration.isAddressValidationEnabled)
            {
                AVA_MAPPER.AddressValidationInfo model = new AVA_MAPPER.AddressValidationInfo();
                model.line1 = avataxConfiguration.Street__c;
                model.line2 = avataxConfiguration.Line2__c;
                model.line3 = avataxConfiguration.Line3__c;
                model.city = avataxConfiguration.City__c;
                model.region = avataxConfiguration.State__c;
                model.postalCode = avataxConfiguration.Postal_Code__c;
                model.country = avataxConfiguration.Country__c;
                if(!Test.isRunningTest())
                {
                    result = client.resolveAddressPost(model);
                }
                else
                {
                    result = new AVA_MAPPER.AddressResolutionModel();
                    result.statusCode = 200;
                }
                if(result.statusCode == 200 || result.statusCode == 201)
                {
                    if(result.validatedAddresses != null)
                    {
                        val.line1 =  result.validatedAddresses[0].line1;
                        val.line2 =  result.validatedAddresses[0].line2;
                        val.line3 =  result.validatedAddresses[0].line3;
                        val.city =  result.validatedAddresses[0].city;
                        val.region =  result.validatedAddresses[0].region;
                        val.postalCode =  result.validatedAddresses[0].postalCode;
                        val.country =  result.validatedAddresses[0].country;
                    }
                }
                else
                {
                    if(result.messages !=null)
                    {
                        val.avError = result.messages[0].details;
                    }
                    else
                    {
                        val.avError = result.error.details[0].message;
                    }
                }
                
            }
            else
            {
                val.avError = AvaConstants.ADDRESS_VALIDATION_DISABLED;
            }
            return val;
        }
        catch(Exception e)
        {
            throw new AVA_MAPPER.AvaTaxException(e.getMessage()+','+e.getStackTraceString());
        }
    }
    
    // AuraEnabled function for storing address
    @AuraEnabled public static AvaTax__c saveAddress(String avataxConfiguration){
        try
        {
            AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
            Map<String, Object> a = (Map<String, Object>)JSON.deserializeUntyped(avataxConfiguration);
            avaTaxInstance.Street__c = String.valueof(a.get('line1'));
            avaTaxInstance.Line2__c = String.valueof(a.get('line2'));
            avaTaxInstance.Line3__c = String.valueof(a.get('line3'));
            avaTaxInstance.City__c = String.valueof(a.get('city'));
            avaTaxInstance.State__c = String.valueof(a.get('region'));
            avaTaxInstance.Postal_Code__c = String.valueof(a.get('postalCode'));
            avaTaxInstance.Country__c = String.valueof(a.get('country'));
            return avaTaxInstance;
        }
        catch(Exception e)
        {
            throw new AVA_MAPPER.AvaTaxException(e.getMessage()+','+e.getStackTraceString());
        }
    } 
    
    // AuraEnabled function for retreiving entity code from AvaTax
    public static List<AVA_MAPPER__Entity_Use_Code__c> entityUseCodeList(AvaTax__c avataxConfiguration){
        
        AvaTaxConfigManager am = new AvaTaxConfigManager();
        am = getavaTaxConfig(avataxConfiguration);
        
        AVA_MAPPER.AvaTaxClient client = new AVA_MAPPER.AvaTaxClient(am.appName, am.appVersion, '',am.environment);
        client.WithSecurity(am.username,am.password);
        
        //Fetch entity use codes from AvaTax API using mappper class
        AVA_MAPPER.FetchResultEntityUseCodeModel entityUseCodeResult = new  AVA_MAPPER.FetchResultEntityUseCodeModel();
        if(!Test.isRunningTest())
        {
            entityUseCodeResult = client.ListEntityUseCodes();
        }
        else
        {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ECResponseResource' LIMIT 1 ];
            String body = sr.Body.toString();
            entityUseCodeResult = (AVA_MAPPER.FetchResultEntityUseCodeModel)System.JSON.deserialize(body,AVA_MAPPER.FetchResultEntityUseCodeModel.class);
        }
        List<AVA_MAPPER__Entity_Use_Code__c> listEntityUseCode = new List<AVA_MAPPER__Entity_Use_Code__c>();
        
        for(AVA_MAPPER.EntityUseCodeModel entityUseCodeValue : entityUseCodeResult.value)
        {
            AVA_MAPPER__Entity_Use_Code__c entityUseCodeObject = new AVA_MAPPER__Entity_Use_Code__c();
            entityUseCodeObject.Name = entityUseCodeValue.code;
            entityUseCodeObject.AVA_MAPPER__Description__c = entityUseCodeValue.name;
            listEntityUseCode.add(entityUseCodeObject);
        }
        return listEntityUseCode;
    }
    
    // AuraEnabled function for retreiving entity code from AvaTax
    public static List<AVACLOUD_SAND__Shipping_Code__c> shippingCodeList(AvaTax__c avataxConfiguration){
        
        AvaTaxConfigManager am = new AvaTaxConfigManager();
        am = getavaTaxConfig(avataxConfiguration);
        
        AVA_MAPPER.AvaTaxClient client = new AVA_MAPPER.AvaTaxClient(am.appName, am.appVersion, '',am.environment);
        client.WithSecurity(am.username,am.password);
        
        //Fetch shipping codes from AvaTax API using mappper class
        AVA_MAPPER.FetchResultTaxCodeModel shippingCodeResult = new AVA_MAPPER.FetchResultTaxCodeModel();
        if(!Test.isRunningTest())
        {
            shippingCodeResult = client.ListTaxCodes('taxCodeTypeId eq \'F\'', 0, 0, 'taxCode');
        }
        List<AVACLOUD_SAND__Shipping_Code__c> listShippingCode = new List<AVACLOUD_SAND__Shipping_Code__c>();
        
        for(AVA_MAPPER.TaxCodeModel shippingTaxCodeValue : shippingCodeResult.value)
        {
            AVACLOUD_SAND__Shipping_Code__c shippingCodeObject = new AVACLOUD_SAND__Shipping_Code__c();
            shippingCodeObject.Name = shippingTaxCodeValue.taxCode;
            shippingCodeObject.Description__c = shippingTaxCodeValue.description;
            listShippingCode.add(shippingCodeObject);
        }
        return listShippingCode;
    }
    
    // AuraEnabled function for Company Code from AvaTax
    public static List<AVACLOUD_SAND__AvaTaxCompany__c>  companyCodeList(AvaTax__c avataxConfiguration){
        
        AvaTaxConfigManager am = new AvaTaxConfigManager();
        am = getavaTaxConfig(avataxConfiguration);
        
        AVA_MAPPER.AvaTaxClient client = new AVA_MAPPER.AvaTaxClient(am.appName, am.appVersion, '',am.environment);
        client.WithSecurity(am.username,am.password);
        AVA_MAPPER.FetchResultCompanyModel companyFetchResult = new AVA_MAPPER.FetchResultCompanyModel();
        
        if(!Test.isRunningTest())
        {
            companyFetchResult = client.QueryCompanies(null,null,null,null,null); 
        }
        else
        {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'CompanyResponseResource' LIMIT 1 ];
            String body = sr.Body.toString();
            companyFetchResult = (AVA_MAPPER.FetchResultCompanyModel)System.JSON.deserialize(body,AVA_MAPPER.FetchResultCompanyModel.class);
        }
        
        List<AVACLOUD_SAND__AvaTaxCompany__c> listCompanyCode = new List<AVACLOUD_SAND__AvaTaxCompany__c>();
        
        for(AVA_MAPPER.CompanyModel companyCodeValue : companyFetchResult.value)
        {
            AVACLOUD_SAND__AvaTaxCompany__c companyCodeObject = new AVACLOUD_SAND__AvaTaxCompany__c();
            companyCodeObject.Name = companyCodeValue.Name;
            companyCodeObject.AVACLOUD_SAND__Avatax_Company_Code__c = companyCodeValue.companyCode;
            listCompanyCode.add(companyCodeObject);
        }
        return listCompanyCode;
    }
    
    @AuraEnabled
    public static Boolean checkMetadataAPI(String url)
    {
        try {
            MetadataService.MetadataPort service = new MetadataService.MetadataPort(url);
            service.SessionHeader = new MetadataService.SessionHeader_element();
            service.SessionHeader.sessionId = UserInfo.getSessionId();
            List<MetadataService.ListMetadataQuery> queries = new List<MetadataService.ListMetadataQuery>();
            MetadataService.ListMetadataQuery remoteSites = new MetadataService.ListMetadataQuery();
            remoteSites.type_x = 'RemoteSiteSetting';
            queries.add(remoteSites);
            service.listMetadata(queries, 28);
        } catch (System.CalloutException e) {
            system.debug('exception : '+ e.getMessage());
            return false;
        }
        return true;
    }
}