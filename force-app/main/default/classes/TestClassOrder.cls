@isTest
private class TestClassOrder {
    private static AvaTax__c avaTaxInstance = null;
    private static Account testAccount = null;
    private static Contact testContact = null;
    private static Order testOrd = null;
    
    //@testSetup
    private testMethod static void testOrder()
    {
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = 'AvaTax Config',
            Account_ID__c = '12345678',
            License_Key__c = 'test',
            Company_Code__c = 'default',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = true,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = true,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Validate_Addresses__c = true,
            AVACLOUD_SAND__Allow_Tax_Override__c = true);
        insert avaTaxInstance;
        
        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax');
        insert settings;
        
        Account testAccount = new Account(Name=DateTime.now().format(), BillingStreet = '900 winlsow way e', BillingCity = 'BI', BillingState='WA', BillingPostalCode = '98110', 
                                          BillingCountry = 'US',ShippingStreet = '', ShippingCity = '', ShippingState='', ShippingPostalCode = '', 
                                          ShippingCountry = '',AVA_MAPPER__Business_Identification_Number__c = '1234');
        insert testAccount;
        
        Contact testContact = new Contact(LastName=DateTime.now().format(), AccountId=testAccount.Id,
                                          MailingStreet = '900 winslow way e', MailingCity = 'BI', MailingState='WA', MailingPostalCode = '98110-2450', MailingCountry = 'US',
                                          OtherStreet = '900 winslow way e', OtherCity = 'BI', OtherState='WA', OtherPostalCode = '98110-2450', OtherCountry = 'US');
        
        insert testContact;
        
        Contract c = new Contract(Name='test',StartDate=Date.Today(),Status = 'Draft', AccountId = testAccount.Id,  ContractTerm = 4);
        insert c;
        
        PriceBook2 pricebookTest = new Pricebook2( Name ='Test PriceBook',
                                                  isActive = true                                               
                                                 );
        insert pricebookTest;
        
        Product2 productTest = new Product2( Name ='Test Product',
                                            Family = 'Hardware',
                                            ProductCode = 'testcode',
                                            Description = 'XYZ',
                                            UPC__c = 'Test UPC'
                                           );
        insert productTest;
        
        Id standardPriceBookId = Test.getStandardPricebookId(); 
        PriceBookEntry pricebookEntryTest = new PricebookEntry( UnitPrice = 500,
                                                               Pricebook2Id = standardPriceBookId,
                                                               Product2Id = productTest.Id,
                                                               isActive = true                                                
                                                              );
        insert pricebookEntryTest;        
        
        Order testOrd = new Order(Name=DateTime.now().format(),Status='Draft',AccountId=testAccount.Id, 
                                  AvaTax_Doc_Status__c = null, AvaTax_Message__c = null, Invoice_Message__c = null,EffectiveDate = Date.Today(),ContractId=c.Id, 
                                  Sales_Tax__c = null, Tax_Date__c=null, PriceBook2Id=standardPriceBookId, AVACLOUD_SAND__AvaTax_Doc_Status__c = 'Committed');
        insert testOrd;
        
        
        testOrd.Sales_Tax__c = 100.00;
        testOrd.tax_date__c = System.today() - 1;
        update testOrd;
        
        OrderItem testOrderLine = new OrderItem(Description='xx',OrderId=testOrd.Id,
                                                PricebookEntryId=pricebookEntryTest.Id,AVACLOUD_SAND__Sales_Tax_Amount_Line__c=100,
                                                AVACLOUD_SAND__Sales_Tax_Details__c='xx\r\n\r\nxx',AVACLOUD_SAND__Sales_Tax_Rate__c='10.00%',AVACLOUD_SAND__Tax_Override__c=true, Quantity=1.0, UnitPrice=12.0);
        insert testOrderLine;
        

        StaticResource sfConfig = [Select id, body from StaticResource Where Name ='SFConfig'];
        System.debug('sfConfig: '+sfConfig);

        ConfigurationProvider config = new ConfigurationProvider();
        AVA_MAPPER.TaxCalculator order = new AVA_MAPPER.TaxCalculator('SFConfig','AVACLOUD_SAND',config.hookExtension());
        
        PageReference pageRef = Page.Order_Calculate_Tax;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testOrd.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(testOrd);
        OrderTaxCalculator gtOrder = new OrderTaxCalculator(sc);
        gtOrder.CalculateTax();
        
        OrderTaxCalculatorHelper gtOrderHelper = new OrderTaxCalculatorHelper(false,'1234');

        List<ProcessBuilderInput> processBuiilderInput = new List<ProcessBuilderInput>();
        
        ProcessBuilderInput pbInputClass = new ProcessBuilderInput();
        pbInputClass.docId = testOrd.Id;
        pbInputClass.docStatus = 'Temporary';
        pbInputClass.commitStatus = 'false';
        processBuiilderInput.add(pbInputClass);

        system.assertEquals(testOrd.AVACLOUD_SAND__AvaTax_Message__c,null);

        OrderTaxCalculator.calculateTax(processBuiilderInput);
        OrderTaxCalculator oppTaxCalc = new OrderTaxCalculator(testOrd.Id);

        system.assertNotEquals(oppTaxCalc, null);
        
        delete testOrd;
    }
}