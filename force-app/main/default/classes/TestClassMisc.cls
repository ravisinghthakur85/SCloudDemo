@isTest
public class TestClassMisc {
    
    @isTest
    private static void companyMappingModelTest(){
        CompanyMappingModel cmm = new CompanyMappingModel();
        cmm.avataxCompanyCode = 'test';
        cmm.subdiaryName='Name';
    }
    
    @isTest
    private static void avaTaxTestConnectionResponseTest(){
        AvaTaxTestConnectionResponse acr = new AvaTaxTestConnectionResponse();
        acr.tcResponse = '';
        acr.defaultValue = '';
        acr.shippingCode = null;
        acr.optionList = null;
        acr.configurationList = null;
        acr.pageSize = 0;
        acr.page = 0;
        acr.total = 0;
        acr.lstMultiCompanyMapping = null;
        acr.countryListing = null;
        acr.cautionString = '';
        acr.customObjName = '';
        acr.displayMultiMapping = false;
        acr.customFieldName = '';
        acr.responseString = '';
        acr.avaTaxConfiguration = null;
    }
    
    @isTest
    private static void configurationProviderTest(){
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = 'AvaTax Config',
            Account_ID__c = '0',
            License_Key__c = 'test',
            Company_Code__c = 'default',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = false,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = true,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Validate_Addresses__c = true,
            Allow_Tax_Override__c = false);
        insert avaTaxInstance;
        
        ConfigurationProvider cp = new configurationProvider();
        cp.getConfig(new AVA_MAPPER.CreateTransactionModel(), new Map<String,List<SObject>>(), new Map<String,List<SObject>>());
    }
    
    @isTest
    public static void testGetAvaTaxDocStatus(){
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = 'AvaTax Config',
            Account_ID__c = '0',
            License_Key__c = 'test',
            Company_Code__c = 'default',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = false,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = false,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Validate_Addresses__c = true,
            Allow_Tax_Override__c = false);
        insert avaTaxInstance;
        
        OrderTaxCalculator.orderId = avaTaxInstance.Id;
        GetAvaTaxDocStatus gStatus = new GetAvaTaxDocStatus();
        Ava_Mapper.TransactionModel transResult = new Ava_Mapper.TransactionModel();
        transResult.statusCode = 200;
        transResult.status = AVA_MAPPER.DocumentStatus.Temporary;
        gstatus.extension(transResult,1);
    }
    
    @isTest
    public static void testGetAvaTaxMessage(){
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = 'AvaTax Config',
            Account_ID__c = '0',
            License_Key__c = 'test',
            Company_Code__c = 'default',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = false,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = false,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Validate_Addresses__c = true,
            Allow_Tax_Override__c = false);
        insert avaTaxInstance;
        
        OrderTaxCalculator.orderId = avaTaxInstance.Id;
        GetAvaTaxMessage gMsg = new GetAvaTaxMessage();
        Ava_Mapper.TransactionModel transResult = new Ava_Mapper.TransactionModel();
        transResult.statusCode = 200;
        gMsg.extension(transResult,1);
    }
}