@isTest  
private class MetadataServiceTest
{    
    /**
     * Dummy Metadata API web service mock class (see MetadataCreateJobTest.cls for a better example)
     **/
	private class WebServiceMockImpl implements WebServiceMock 
	{
		public void doInvoke(
			Object stub, Object request, Map<String, Object> response,
			String endpoint, String soapAction, String requestName,
			String responseNS, String responseName, String responseType) 
		{
			if(request instanceof MetadataService.listMetadata_element)
            {
				response.put('response_x', new MetadataService.listMetadataResponse_element());
            }
			
			return;
		}
	}    
		
	@IsTest
	private static void coverGeneratedCodeCRUDOperations()
	{	
    	// Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        MetadataService metaDataService = new MetadataService();
        // Invoke operations     
        Test.startTest();    
        MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort('https://na59.salesforce.com');
        Test.stopTest();
        System.assertNotEquals(null, metaDataPort);
	}
	
	@IsTest
    private static void coverGeneratedCodeFileBasedOperations1()
    {    	
    	// Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        MetadataService metaDataService = new MetadataService();
        // Invoke operations    
        Test.startTest();     
        MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort('https://na59.salesforce.com');
        metaDataPort.listMetadata(null, null);
        Test.stopTest();
        System.assertNotEquals(null, metaDataPort);
    }
    
    	@IsTest
    private static void coverGeneratedCodeTypes()
    {    	       
        // Reference types
        Test.startTest();
        new MetadataService();
        new MetadataService.listMetadataResponse_element();
        new MetadataService.FileProperties();
        new MetadataService.ListMetadataQuery();
        new MetadataService.listMetadata_element();
        new MetadataService.Metadata();
        new MetadataService.SessionHeader_element();
        new MetadataService.RemoteSiteSetting();
        new MetadataService.ReadRemoteSiteSettingResult().getRecords();
        new MetadataService.readRemoteSiteSettingResponse_element().getResult();
        Test.stopTest();
        System.assertNotEquals(NULL,new MetadataService());
    }
}