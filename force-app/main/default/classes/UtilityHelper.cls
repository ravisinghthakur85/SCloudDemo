public with sharing class UtilityHelper	{

    public static AVA_MAPPER.AddressLocationInfo generateOriginAddress()
    {
        AvaTax__c settings = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        AVA_MAPPER.AddressLocationInfo origin = new AVA_MAPPER.AddressLocationInfo();
        origin.line1 = settings.Street__c;
        origin.line2 = settings.Line2__c;
        origin.line3 = settings.Line3__c;
        origin.city = settings.City__c;
        origin.region = settings.State__c;
        origin.postalCode = settings.Postal_Code__c;
        origin.country = settings.Country__c;
        return origin;
    }
    
    public static AVA_MAPPER__Entity_Use_code__c fetchEntityUseCode(string exemptEntity)
    {
        if(null != exemptEntity)
        {
            List<AVA_MAPPER__Entity_Use_code__c> loEntityUseCode = [select Id,Name, AVA_MAPPER__Description__c from AVA_MAPPER__Entity_Use_code__c where Id =: exemptEntity];
            if(loEntityUseCode.size() > 0)
            {
                return loEntityUseCode[0];
            }
        }
        return new AVA_MAPPER__Entity_Use_code__c(Name='',AVA_MAPPER__Description__c='');
    }
    
    public static string trimData(String fieldName, Integer fieldLength)
    {
        if(fieldName != null)
        {
            if(fieldName.Length()>fieldLength)
            {
                return fieldName.substring(0,fieldLength);
            }
            else
            {
                return fieldName;
            }
        }
        else
        {
            return '';
        }
    }
    
    public static string CreateGetTaxLogs(AVA_MAPPER.TransactionModel result, string functionName,string connectorTime, string source,string operation,string type,string getTaxException) {
        string logs;
        string avataxDomainServiceEnvironment = getAvaTaxEnvironment();
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        
        if (getTaxException == null) {
            logs = '[' + '"CallerAccuNum": ' + '"' + avaTaxInstance.Account_ID__c + '"' +
                ',"LogType":"Performance","LogLevel":"Informational","ConnectorName":'+'"'+ AvaConstants.AVATAX_CONNECTOR_NAME+'",'+
                '"ConnectorVersion":"'+ AvaConstants.AVATAX_CONNECTOR_VERSION +'",' + '"DocCode"' + ':' + '"' + result.code + '",' +
                '"Operation":"'+operation+'","ServiceURL":' + avataxDomainServiceEnvironment + ',"Source":'+source+',' + '"LineCount"' + ':' + '"' + result.lines.size() + '",' +
                +'"DocType"' + ':' + '"' + result.type + '",' + '"EventBlock":"PostGetTax","FunctionName":'+'"'+functionName+'",' +
                '"ConnectorTime":' + '"' + connectorTime + '",' + '"ConnectorLatency":' + '"' + result.latencyTime + '",' +
                
                '"Message":' + + '"'+' CONNECTORMETRICS, TYPE - ' + type +',' +
                'DocCode - ' + result.code + ',' + 'Line Count - ' + result.lines.size() + ',' +
                'Connector Time - ' + ' ' + connectorTime + ',' + 'Connector Latency - ' + result.latencyTime + '"'+ ']';
        } else {
            logs = '[' + '"CallerAccuNum" : ' + '"' + avaTaxInstance.Account_ID__c + '"' +
                ',"LogType":"Debug",  "LogLevel":"Exception","ConnectorName":'+'"'+AvaConstants.AVATAX_CONNECTOR_NAME+'",'+
                '"ConnectorVersion":"'+ AvaConstants.AVATAX_CONNECTOR_VERSION +'",'
                + '"Operation":"CreateTransaction","ServiceURL":"'+avataxDomainServiceEnvironment+'","Source":'+source+',"FunctionName":"'+functionName+'",' +
                '"Message":' + '"' + getTaxException + '"' + ']';
            
        }
        return logs;
        
    }
    
    public static string createTestConnectionLogs(string logType,string source,string operation, string logLevel, AvaTax__c message,string functionName,string configurationException) {
        string logs;
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        string avataxDomainServiceEnvironment = getAvaTaxEnvironment();
        if(configurationException == null )
        {
            logs = '[' + '"CallerAccuNum":' + '"' + avaTaxInstance.Account_ID__c + '"' 
                + ',"LogType":' + logType + ',"LogLevel":' + logLevel + ',"ConnectorName":'+'"'+AvaConstants.AVATAX_CONNECTOR_NAME+'",'+
                '"ConnectorVersion":"'+ AvaConstants.AVATAX_CONNECTOR_VERSION +'",' 
                + '"Operation":"'+operation+'","ServiceURL":"' + avataxDomainServiceEnvironment + '","FunctionName":"'+functionName+'","Message":' 
                +'"' + getMessageStringForLogEntries( message) + '"' +']';
        }
        else {
            logs = '[' + '"CallerAccuNum" : ' + '"' + avaTaxInstance.Account_ID__c + '"' +
                ',"LogType":"Debug",  "LogLevel":"Exception","ConnectorName":'+'"'+AvaConstants.AVATAX_CONNECTOR_NAME+'",'+
                '"ConnectorVersion":"'+ AvaConstants.AVATAX_CONNECTOR_VERSION +'",' 
                + '"Operation":"'+operation+'","ServiceURL":"'+avataxDomainServiceEnvironment+'","Source":'+source+',"FunctionName":"'+functionName+'",' +
                '"Message":' + '"' + configurationException + '"' + ']';
            
        }
        return logs;
    }
    
    public static string getMessageStringForLogEntries(AvaTax__c avaTaxSettingCustomSetting)
    {
        string logEntriesString = '';
        if(String.isNotBlank(avaTaxSettingCustomSetting.AVACLOUD_SAND__Account_ID__c))
        {
            logEntriesString += 'Account ID - ' + avaTaxSettingCustomSetting.AVACLOUD_SAND__Account_ID__c +',';
        }
        if(avaTaxSettingCustomSetting.AVACLOUD_SAND__Sandbox__c)
        {
            logEntriesString += 'Sandbox - checked'  +',';
        }
        else
        {
            logEntriesString += 'Sandbox - unchecked'  +',';
        } 
        
        //  logEntriesString += 'Sandbox - ' + avaTaxSettingCustomSetting.AVACLOUD_SAND__Sandbox__c +',';
        if(String.isNotBlank(avaTaxSettingCustomSetting.AVACLOUD_SAND__Company_Code__c))
        {
            logEntriesString += 'Company Code - ' + avaTaxSettingCustomSetting.AVACLOUD_SAND__Company_Code__c +',';
        }
        if(String.isNotBlank(avaTaxSettingCustomSetting.AVACLOUD_SAND__Street__c))
        {
            logEntriesString += 'Line 1 - ' + avaTaxSettingCustomSetting.AVACLOUD_SAND__Street__c +',';
        }
        if(String.isNotBlank(avaTaxSettingCustomSetting.AVACLOUD_SAND__Line2__c))
        {
            logEntriesString += 'Line 2 - ' + avaTaxSettingCustomSetting.AVACLOUD_SAND__Line2__c +',';
        }
        if(String.isNotBlank(avaTaxSettingCustomSetting.AVACLOUD_SAND__Line3__c))
        {
            logEntriesString += 'Line 3 - ' + avaTaxSettingCustomSetting.AVACLOUD_SAND__Line2__c +',';
        }
        if(String.isNotBlank(avaTaxSettingCustomSetting.AVACLOUD_SAND__City__c))
        {
            logEntriesString += 'City - ' + avaTaxSettingCustomSetting.AVACLOUD_SAND__City__c +',';
        } 
        if(String.isNotBlank(avaTaxSettingCustomSetting.AVACLOUD_SAND__State__c))
        {
            logEntriesString += 'State - ' + avaTaxSettingCustomSetting.AVACLOUD_SAND__State__c +',';
        }
        if(String.isNotBlank(avaTaxSettingCustomSetting.AVACLOUD_SAND__Postal_Code__c))
        {
            logEntriesString += 'Postal Code - ' + avaTaxSettingCustomSetting.AVACLOUD_SAND__Postal_Code__c +',';
        }
        if(String.isNotBlank(avaTaxSettingCustomSetting.AVACLOUD_SAND__Country__c))
        {
            logEntriesString += 'Country - ' + avaTaxSettingCustomSetting.AVACLOUD_SAND__Country__c +',';
        }
        if(String.isNotBlank(avaTaxSettingCustomSetting.AVACLOUD_SAND__Shipping_Code__c))
        {
            logEntriesString += 'Shipping Code - ' + avaTaxSettingCustomSetting.AVACLOUD_SAND__Shipping_Code__c +',';
        }
        if(avaTaxSettingCustomSetting.AVACLOUD_SAND__Enable_AvaTax_Tax_Calculation__c)
        {
            logEntriesString += 'Enable AvaTax Tax Calculation - checked'  +',';
        }
        else
        {
            logEntriesString += 'Enable AvaTax Tax Calculation - unchecked'  +',';
        }
        if(avaTaxSettingCustomSetting.AVACLOUD_SAND__Enable_UPC_Code_as_ItemCode__c)
        {
            logEntriesString += 'Enable UPC Code as Item Code - checked'  +',';
        }
        else
        {
            logEntriesString += 'Enable UPC Code as Item Code - unchecked'  +',';
        }
        if(avaTaxSettingCustomSetting.AVACLOUD_SAND__Save_transactions_to_AvaTax__c)
        {
            logEntriesString += 'Save transactions to AvaTax - checked'  +',';
        }
        else
        {
            logEntriesString += 'Save transactions to AvaTax - unchecked'  +',';
        }        
        if(avaTaxSettingCustomSetting.AVACLOUD_SAND__Allow_Tax_Override__c)
        {
            logEntriesString += 'Allow Tax Override - checked'  +',';
        }
        else
        {
            logEntriesString += 'Allow Tax Override - unchecked'  +',';
        } 
        if(avaTaxSettingCustomSetting.AVACLOUD_SAND__Validate_Addresses__c)
        {
            logEntriesString += 'Validate Addresses - checked'  +',';
        }
        else
        {
            logEntriesString += 'Validate Addresses - unchecked'  +',';
        }       
        return logEntriesString;
    }
    
    public static string getLogEntriesToken()
    {
        string logEntriesSecurityToken ='';
        string environment = getAvaTaxEnvironment();
        if(environment == 'Production')
        {
            logEntriesSecurityToken = AvaConstants.AVATAX_LOGENTRIESTOKEN_PRODUCTION;
        }
        else
        {
            logEntriesSecurityToken = AvaConstants.AVATAX_LOGENTRIESTOKEN_SANDBOX;
        }
        
        return logEntriesSecurityToken;
    }
    
    public static string getAvaTaxEnvironment()
    {
        string avataxDomainServiceEnvironment;
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        if (avaTaxInstance.Sandbox__c != true) {
            avataxDomainServiceEnvironment = String.valueOf(AVA_MAPPER.AvaTaxEnvironment.Production);
        } else {
            avataxDomainServiceEnvironment = String.valueOf(AVA_MAPPER.AvaTaxEnvironment.Sandbox);
        }
        return avataxDomainServiceEnvironment;
    }
    
    public static string trimNewLineCharacterFromString(String str)
    {
        str = str.replace('\r\n', '');
        str = str.replace('\n', '');
        str = str.replace('\r', '');
        return str;
    }
    
    public static void setShipFromAddressAndCompany(String subsidiaryId, AVA_MAPPER.CreateTransactionModel model)
    {
        System.debug('SubsidiaryId: '+SubsidiaryId);
            
        List<MultiCompanyMapping__c> multiCompanyMappingData = new List<MultiCompanyMapping__c>();
        AVA_MAPPER.AddressLocationInfo shipfromAddress = new AVA_MAPPER.AddressLocationInfo();
        String companyCode;

        if (Schema.sObjectType.MultiCompanyMapping__c.fields.Name.isUpdateable()) {
            
            multiCompanyMappingData = [Select AvaCompanyCode__c, SubsidiaryName__c
                                       from MultiCompanyMapping__c where SubsidiaryName__c = : subsidiaryId];
        }
        
        System.debug('multiCompanyMappingData: '+multiCompanyMappingData);
        if (multiCompanyMappingData.size() != 0 && string.isNotBlank(multiCompanyMappingData[0].AvaCompanyCode__c))
        {
            List<AvaTaxCompany__c> selectedCompanyAddress = new List<AvaTaxCompany__c>();
            if (Schema.sObjectType.AvaTaxCompany__c.fields.Name.isUpdateable()) {
                
                selectedCompanyAddress =  [Select AddressLine1__c, AddressLine2__c, 
                                           City__c, State__c, 
                                           Country__c, PostalCode__c from AvaTaxCompany__c 
                                           where Avatax_Company_Code__c =:  multiCompanyMappingData[0].AvaCompanyCode__c];
            }
            
            System.debug('selectedCompanyAddress: '+selectedCompanyAddress);
            if(string.isNotBlank(selectedCompanyAddress[0].AddressLine1__c) ||
               string.isNotBlank(selectedCompanyAddress[0].AddressLine2__c) ||
               string.isNotBlank(selectedCompanyAddress[0].City__c) ||
               string.isNotBlank(selectedCompanyAddress[0].State__c) ||
               string.isNotBlank(selectedCompanyAddress[0].Country__c) ||
               string.isNotBlank(selectedCompanyAddress[0].PostalCode__c) )
            {
                shipfromAddress = generateSubsidiaryOriginAddress(selectedCompanyAddress[0]);
            	System.debug('In subsidiary shipfromAddress');
            }
            else
            {
                shipfromAddress = generateOriginAddress();
            	System.debug('In default shipfromAddress');
            }
            companyCode = string.valueof(multiCompanyMappingData[0].AvaCompanyCode__c);
        }
        else
        {
            shipfromAddress = generateOriginAddress();
            companyCode = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Company_Code__c;
        }

        if(model.addresses.shipFrom == null && model.addresses.singleLocation == null && model.addresses.pointOfOrderAcceptance == null 
           && model.addresses.pointOfOrderOrigin == null)
        {
            System.debug('In shipfrom 1');
            model.addresses.shipFrom = shipfromAddress;
        }
        
        if(model.addresses.shipFrom != null && model.addresses.shipFrom.city == null && model.addresses.shipFrom.country == null
           && model.addresses.shipFrom.line1 == null && model.addresses.shipFrom.line2 == null && model.addresses.shipFrom.line3 == null
           && model.addresses.shipFrom.locationCode == null && model.addresses.shipFrom.postalCode == null && model.addresses.shipFrom.region == null)
        {
            System.debug('In shipfrom 2');
            model.addresses.shipFrom = shipfromAddress;
        }
        
        //model.addresses.shipFrom = shipfromAddress;
        model.companyCode = companyCode;

        System.debug('shipfromAddress: '+shipfromAddress);
        System.debug('model.companyCode: '+companyCode);
    }
    
    
    public static AVA_MAPPER.AddressLocationInfo generateSubsidiaryOriginAddress(SObject companyAddress)
    {
        AVA_MAPPER.AddressLocationInfo originAddress = new AVA_MAPPER.AddressLocationInfo();
        originAddress.line1 = string.valueOf(getFieldValue(companyAddress,'AddressLine1__c'));
        originAddress.city = string.valueOf(getFieldValue(companyAddress,'City__c'));
        originAddress.region =  string.valueOf(getFieldValue(companyAddress,'State__c'));
        originAddress.postalCode = string.valueOf(getFieldValue(companyAddress,'PostalCode__c'));
        originAddress.country = string.valueOf(getFieldValue(companyAddress,'Country__c'));
        return originAddress;
    }
    
    public static list<Id> listGetOrderId(list<Order> orderList)
    {
        List<Id> orderIds = new List<Id>();
        for(Order order: orderList)
        {
            orderIds.add(Order.Id);
        } 
        return orderIds;
    }
    
    static public Object getFieldValue(SObject o,String field) {
        System.debug('O: '+o);
        System.debug('field: '+field);
        if(field.contains('.')) {
            String nextField = field.substringAfter('.');
            String relation = field.substringBefore('.');
            return getFieldValue((SObject)o.getSObject(relation),nextField);
        }
        else {
            return o.get(field);
        }        
    }
    
    //Added for Freight code list
    public static void updateFreightCodes(List<AVACLOUD_SAND__Shipping_Code__c> listFreightCode){
        List<AVACLOUD_SAND__Shipping_Code__c> existingFreightCode = [select name,description__c from AVACLOUD_SAND__Shipping_Code__c limit 1000];
        List<AVACLOUD_SAND__Shipping_Code__c> newFreightCodes = new List<AVACLOUD_SAND__Shipping_Code__c>();
        for(AVACLOUD_SAND__Shipping_Code__c newCode : listFreightCode){
            Boolean isNew = true;
            
            for(AVACLOUD_SAND__Shipping_Code__c existingCode : existingFreightCode){
                if(newCode.Name == existingCode.Name){
                    existingCode.Description__c = newCode.Description__c;
                    isNew = false;
                    break;
                }
            }
            if(isNew){
                newFreightCodes.add(newCode);
            }
        }
        if(existingFreightCode.size()>0){
            update existingFreightCode;
        }
        if(newFreightCodes.size()>0){
            insert newFreightCodes;
        }
    }
}