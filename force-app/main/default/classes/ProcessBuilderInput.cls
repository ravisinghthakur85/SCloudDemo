public class ProcessBuilderInput {
    @InvocableVariable(required=true)
    public ID docId;
    @InvocableVariable(required=false)
    public String docStatus;    
    @InvocableVariable(required=false)
    public String commitStatus;    
}