global With Sharing class OpportunityTaxCalculator
{
    public static Id opptyId = null;
    public static DateTime connectorTimeStart;
    public static DateTime connectorTimeRequestFetchStop;
    public static DateTime connectorTimeResponseUpdateStart;
    public static DateTime connectorTimeStop;
    public static long connectorFetchTime;
    public static string logs;
    
    public OpportunityTaxCalculator(ApexPages.StandardController controller) 
    {
        opptyId = controller.getRecord().id;    
    }
    
    public OpportunityTaxCalculator(Id currentOpptyId)
    {
        opptyId = currentOpptyId;
    }
    
    public PageReference calculateTax()
    {
        Map<string, string> taxCalcParam = new Map<string, string>();
        taxCalcParam.put('docId', opptyId);
        connectorTimeStart = system.now();
        calculateTax(taxCalcParam);
        return redirectPage();
    }
    
    @InvocableMethod
    public static void calculateTax(List<ProcessBuilderInput> opportunityFieldValues)
    {
        //taxCalculationParameter.put('docId', OrderId[0]);
        for(processBuilderInput opportunityFieldValue : opportunityFieldValues)
        {
            Map<string, string> taxCalculationParameter = new Map<string, string>();
            taxCalculationParameter.put('docId', opportunityFieldValue.docId);
            taxCalculationParameter.put('docStatus', 'False');
            taxCalculationParameter.put('commitStatus', 'False');
            asyncCalculateTax(taxCalculationParameter);
        }
    }
    
    @future (callout = true)
    static public void asyncCalculateTax(map<string, string> taxCalculationParameter)
    {
        connectorTimeStart = system.now();
        calculateTax(taxCalculationParameter);
    }
    
    static public void calculateTax(Map<string, string> taxCalculationParameter)
    {
        string sourceName = '';
        if(system.isFuture())
        {
            sourceName = 'GetTaxOpportunity.asyncCalculateTax()';
        }
        else
        {
            sourceName = 'GetTaxOpportunity.CalculateTax()';
        }
        
        try{
            string opptyDocId = taxCalculationParameter.get('docId');
            opptyId = opptyDocId;
            
            if(String.isBlank(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).AVACLOUD_SAND__Account_ID__c) || 
              String.isBlank(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).AVACLOUD_SAND__License_Key__c))
            {
                Throw new SalesCloudAvaTaxException(AvaConstants.AVATAX_CREDENTIALS_ERROR);
            }

            if(!AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Enable_AvaTax_Tax_Calculation__c)
            {
                Throw new SalesCloudAvaTaxException(AvaConstants.TAX_CALCULATION_DISABLED);
            }
            else
            {
                Map<String,String> opptyParam = new Map<String,String>();
                ConfigurationProvider config = new ConfigurationProvider();
                
                string subsidiaryNameLocal='';
                string subsidiaryNameInDBMapping = '';
                //string orgNamespace = '';
                
                if(String.isNotBlank(AVACLOUD_SAND__AdditionalSettings__c.getValues('AvaTax').CustomFieldApiName__c))
                {
                	//orgNamespace = [SELECT NamespacePrefix FROM Organization LIMIT 1].NamespacePrefix;
                    subsidiaryNameInDBMapping = AVACLOUD_SAND__AdditionalSettings__c.getValues('AvaTax').CustomFieldApiName__c;
                
                    System.debug('subsidiaryNameInDBMapping: '+subsidiaryNameInDBMapping);

                    /*if(!subsidiaryNameInDBMapping.containsIgnoreCase(orgNamespace))
                    {
                        subsidiaryNameInDBMapping = orgNamespace+'__'+subsidiaryNameInDBMapping;
                    }*/

                    if(string.isNotBlank(subsidiaryNameInDBMapping))
                    {
                        subsidiaryNameLocal = 'Account.'+subsidiaryNameInDBMapping;
                    }
                }

                AVA_MAPPER.TaxCalculator oppty = new AVA_MAPPER.TaxCalculator(AvaConstants.STATIC_RESOURCE_FILENAME,AvaConstants.AVATAX_PACKAGE_NAMESPACE,config.hookExtension()); 
                oppty.setCustomBusinessCaseListener(new OpportunityTaxCalculatorHelper(subsidiaryNameLocal));
                
                if(String.isBlank(subsidiaryNameInDBMapping))
                {
                    opptyParam.put('subsidiary',null);
                }
                else
                {
                    opptyParam.put('subsidiary',', Opportunity.Account.' + subsidiaryNameInDBMapping);
                }

                if(UserInfo.isMultiCurrencyOrganization())
                {
                    opptyParam.put('currencyIsoCode',', CurrencyIsoCode');
                }
                
                AVA_MAPPER.TaxCalculationInput taxCalcInput = new AVA_MAPPER.TaxCalculationInput();
                taxCalcInput.recordId = opptyDocId;
                taxCalcInput.controller = 'opportunity';
                taxCalcInput.optionalParams = opptyParam;
                taxCalcInput.isMultiCommit = False; 
                connectorTimeRequestFetchStop = System.now();
                connectorFetchTime = connectorTimeRequestFetchStop.getTime() - connectorTimeStart.getTime();
                
                Ava_Mapper.TransactionModel transResult = oppty.calculateTax(taxCalcInput);
                connectorTimeResponseUpdateStart = System.now();
                ConnectorTimeStop = System.now();
                if(transResult.statusCode == 200 || transResult.statusCode == 201)
                {
                    long connectorUpdateTime = ConnectorTimeStop.getTime() - ConnectorTimeResponseUpdateStart.getTime();
                    long connectorTime = connectorUpdateTime +connectorFetchTime;
                    logs = UtilityHelper.CreateGetTaxLogs(transResult, 'CalculateTax()',string.valueof(connectorTime), sourceName,'CreateTransaction','GetTax', null);
                    system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));
                }                
                else if(transResult.statusCode == 501)
                {
                    system.debug('transResult.error.message: '+transResult.error.message);
                    logs = UtilityHelper.CreateGetTaxLogs(transResult, 'CalculateTax()',null, sourceName,null,null, transResult.error.message);
                    logs = UtilityHelper.trimNewLineCharacterFromString(logs);
                    system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));
                }
            }
        }
        catch(Exception e)
        {
            logs = UtilityHelper.CreateGetTaxLogs(null, 'CalculateTax()',null, sourceName,null,null, string.valueOf(e.getMessage()+','+e.getStackTraceString()));
            logs = UtilityHelper.trimNewLineCharacterFromString(logs);
            system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));

            Opportunity updateOpportunity = new Opportunity(Id=opptyId);
            if (Schema.sObjectType.Opportunity.fields.AvaTax_Message__c.isUpdateable()){
                updateOpportunity.AvaTax_Message__c = e.getMessage()+','+e.getStackTraceString();
                update updateOpportunity;
            }
        }
    }
    
    public PageReference redirectPage() {
        // Redirect the user back to the original page         
        PageReference pageRef = new PageReference('/' + opptyId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}