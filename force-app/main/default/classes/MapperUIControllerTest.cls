@isTest
private class MapperUIControllerTest {
	@isTest
    private static void testDisplayObjectDetail(){
        MapperUIController.displayObjectDetail();
    }
    
    @isTest
    private static void testLoadXml(){
        MapperUIController.loadXml();
    }
    
    @isTest
    private static void testGenerateNewHookClass(){
        string className = 'HookTest';
        string classContent = 'public HookTest() { } ';
        MapperUIController.generateNewHookClass(className, classContent);
    }
    
    @isTest
    private static void testGetXmlString(){
        MapperUIController.getXmlString('TestData');
    }
    
    @isTest
    private static void testverifySql(){
        String query = 'SELECT id from Account limit 1';
        MapperUIController.verifySql(query);
        MapperUIController.verifySql('query');
    }
    
    @isTest
    private static void testFetchOrgNamespace(){
        MapperUIController.fetchOrgNamespace();
    }
    
    @isTest
    private static void testGetListofUpdateTaxHelperNames(){
        MapperUIController.getListofUpdateTaxHelperNames();
    }
    
    @isTest
    private static void testMapperUIControllerVars(){
        MapperUIController.jsonData = '';
        MapperUIController.classContent = '';
        MapperUIController.className = '';
        MapperUIController.classResponse = '';
        MapperUIController.xmlString = '';
        MapperUIController muc = new MapperUIController();
        //muc.allObjList = new List<ObjectClass>();
        muc.allobjectList = new List<String>();
        muc.objectSet = new Set<String>();
    }
}