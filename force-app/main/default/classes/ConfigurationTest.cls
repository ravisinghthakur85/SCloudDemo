@isTest
public class ConfigurationTest {
    
    @TestSetup
    static void setup()
    {
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = AvaConstants.AVATAX_CUSTOM_SETTING_NAME,
            Account_ID__c = '111111111',
            License_Key__c = '111111111',
            Company_Code__c = 'Test',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = true,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = true,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Line2__c = 'test',
            Line3__c = 'test',
            Validate_Addresses__c = true);
        insert avaTaxInstance;
    }
    
    @isTest
    public static void testOnConnect()
    {
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        AvaTaxTestConnectionResponse response = AvaTaxConfigurationController.onConnect(avaTaxInstance);
        system.assertNotEquals(response, null);
    }
    
    @isTest
    public static void testgetAvataxConfiguration()
    {
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        List<Shipping_Code__c> shippingCodeList = new List<Shipping_Code__c> {new Shipping_Code__c(Name = 'Test')};
        insert shippingCodeList;
        List<AvaTaxCompany__c> companyList = new List<AvaTaxCompany__c> {new AvaTaxCompany__c(Name = 'Test', AvaTax_Company_Code__c='Test')};
        insert companyList;
            AvaTaxTestConnectionResponse response = AvaTaxConfigurationController.getAvataxConfiguration();
        system.assertNotEquals(response, null);
    }
    
    @isTest
    public static void testgetMultiCompanyMapping()
    {
        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax', 
                                                                   CustomObjectApiName__c = 'Contact',
                                                                   CustomFieldApiName__c = 'Name');
        insert settings;
        
        AvaTaxCompany__c aComp = new AvaTaxCompany__c(Avatax_Company_Code__c = 'test',
                                                      AddressLine1__c = 'test', 
                                                      AddressLine2__c = 'test', 
                                                      City__c = 'test', 
                                                      State__c = 'test', 
                                                      PostalCode__c = 'test', 
                                                      Country__c = 'test');
        insert aComp;
        Contact c = new Contact(LastName = 'Test');
        insert c;
        MultiCompanyMapping__c mc = new MultiCompanyMapping__c(SFCompanyId__c = '',SubsidiaryName__c = c.Id, AvaCompanyCode__c = 'test');
        insert mc;
        List<MultiCompanyModel> response = AvaTaxConfigurationController.getMultiCompanyMapping('Contact','Name');
        system.assertNotEquals(response, null);
    }
    
    @isTest
    public static void testgetMultiCompanyMappingNullAddress()
    {
        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax', 
                                                                   CustomObjectApiName__c = 'Contact',
                                                                   CustomFieldApiName__c = 'Name');
        insert settings;
        
        AvaTaxCompany__c aComp = new AvaTaxCompany__c(Avatax_Company_Code__c = 'test');
        insert aComp;
        Contact c = new Contact(LastName = 'Test');
        insert c;
        MultiCompanyMapping__c mc = new MultiCompanyMapping__c(SFCompanyId__c = '',SubsidiaryName__c = c.Id, AvaCompanyCode__c = 'test');
        insert mc;
        List<MultiCompanyModel> response = AvaTaxConfigurationController.getMultiCompanyMapping('Contact','Name');
        system.assertNotEquals(response, null);
    }
    
    @isTest
    public static void testSaveAcc()
    {
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        AvaTaxTestConnectionResponse avaTaxResponse = new AvaTaxTestConnectionResponse();
        avaTaxResponse = AvaTaxConfigurationController.saveAvataxConfiguration(avaTaxInstance,'abc','abc','abc','abc','abc','abc');
        system.assertNotEquals(avaTaxResponse, null);
    }
    
    @isTest
    public static void testSaveIncorrecAcc()
    {
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        avaTaxInstance.Account_ID__c ='';
        
        AvaTaxTestConnectionResponse avaTaxResponse = new AvaTaxTestConnectionResponse();
        
        avaTaxResponse = AvaTaxConfigurationController.saveAvataxConfiguration(avaTaxInstance,'abc','abc','abc','abc','abc','abc');
        system.assertNotEquals(avaTaxResponse, null);
    }
    
    @isTest
    public static void testSaveAddr()
    {
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        AvaTax__c response = AvaTaxConfigurationController.saveAddress(JSON.serialize(avaTaxInstance));
        system.assertNotEquals(response, null);
    }
    
    @isTest
    public static void testAddrVal()
    {
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        AddressValidationParams ap = new AddressValidationParams();
        ap.city ='';
        ap.country ='';
        ap.line1 ='';
        ap.line2 ='';
        ap.line3 ='';
        ap.avError ='';
        ap.region ='';
        ap.postalCode ='';
        
        AddressValidationParams response = AvaTaxConfigurationController.addressValidation(avaTaxInstance);
        system.assertNotEquals(response, null);
    }
    
    
    @isTest
    public static void testWrongAddrVal()
    {
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        AddressValidationParams ap = new AddressValidationParams();
        ap.city ='';
        ap.country ='';
        ap.line1 ='';
        ap.line2 ='';
        ap.line3 ='';
        ap.avError ='';
        ap.region ='';
        ap.postalCode ='123';
        
        AddressValidationParams response = AvaTaxConfigurationController.addressValidation(avaTaxInstance);
        system.assertNotEquals(response, null);
    }
    
     @isTest
    private static void onConnectTest(){
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = 'AvaTax Config',
            Account_ID__c = '',
            License_Key__c = '',
            Company_Code__c = 'default',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = false,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = true,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Validate_Addresses__c = true,
            Allow_Tax_Override__c = false);
        insert avaTaxInstance;

		AvaTaxConfigurationController.onConnect(avaTaxInstance);
        try{
        AvaTaxConfigurationController.onConnect(null);
        }
        catch(Exception ex){
            System.debug('In exception');
        }
    }
    
    @isTest
    private static void upsertAddiionalSettingsTest(){
        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax');
        insert settings;
        AvaTaxConfigurationController.upsertAddiionalSettings('Contact', 'Name');
    }
    
    @isTest
    private static void getAvaTaxMultiEntityConfigurationTest(){
        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax', CustomObjectApiName__c = 'Contact',CustomFieldApiName__c = 'Name');
        insert settings;
        AvaTaxConfigurationController.getAvaTaxMultiEntityConfiguration(null, null);
        AvaTaxConfigurationController.getAvaTaxMultiEntityConfiguration('Contact', 'Name');
    }
    
}