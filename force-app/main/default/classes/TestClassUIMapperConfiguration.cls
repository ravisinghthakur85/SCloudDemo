@isTest
public class TestClassUIMapperConfiguration {
    private testMethod static void testStaticResourceConfig()
    {
        //Create your customsied xml file & store in the static resource
        //StaticResource sr = [SELECT id, body from StaticResource where name = 'Your_SFConfig_Modified' and namespaceprefix = 'Your_Package_Namespace' LIMIT 1];
        SalesCloudUIMapperConfiguration salesConfig = new SalesCloudUIMapperConfiguration();
        //Below line will update the connector static resource xml file - SFConfig.xml
        salesConfig.writeData('test data');
        
        AVA_MAPPER.MapperConfigurationResult mapperResult = new AVA_MAPPER.MapperConfigurationResult();
        mapperResult = salesConfig.readData();

		system.assertNotEquals(mapperResult, null);
    }
}