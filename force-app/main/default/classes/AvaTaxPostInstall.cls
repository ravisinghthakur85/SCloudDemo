public class AvaTaxPostInstall implements InstallHandler 
{
    public void onInstall(InstallContext context)
    {
        if(context.previousVersion() == null)   // first install - create a new AvaTax configuration Record
        {
            List<AggregateResult> avaTaxSettingsCheck = [select count(Id) from AVACLOUD_SAND__AvaTax__c where name = :AvaConstants.AVATAX_CUSTOM_SETTING_NAME];
            
            Integer avaTaxSettingsCount = (Integer) avaTaxSettingsCheck[0].get('expr0');
            
            if(avaTaxSettingsCount == 0)
            {
                if (Schema.sObjectType.AVACLOUD_SAND__AvaTax__c.isAccessible() && (Schema.sObjectType.AVACLOUD_SAND__AvaTax__c.isCreateable())) 
                {
                    AVACLOUD_SAND__AvaTax__c avaTaxSettings = new AVACLOUD_SAND__AvaTax__c();

                    if (Schema.sObjectType.AVACLOUD_SAND__AvaTax__c.fields.Name.isCreateable())
                    {
                        avaTaxSettings.Name=AvaCOnstants.AVATAX_CUSTOM_SETTING_NAME;
                    }

                    if(Schema.sObjectType.AVACLOUD_SAND__AvaTax__c.isCreateable())
                    {
                        insert avaTaxSettings;
                    }
                }
            }
            
            List<AggregateResult> additionalAvaTaxSettingsCheck = [select count(Id) from AVACLOUD_SAND__AdditionalSettings__c where name = :AvaConstants.AVATAX_ADDITIONAL_CUSTOM_SETTING_NAME];
            
            Integer additionalAvaTaxSettingsCount = (Integer) additionalAvaTaxSettingsCheck[0].get('expr0');
            
            if(additionalAvaTaxSettingsCount == 0 && Schema.sObjectType.AVACLOUD_SAND__AdditionalSettings__c.isAccessible() 
               && (Schema.sObjectType.AVACLOUD_SAND__AdditionalSettings__c.isCreateable()) 
               && Schema.sObjectType.AVACLOUD_SAND__AdditionalSettings__c.fields.Name.isCreateable()) 
            {
                AVACLOUD_SAND__AdditionalSettings__c avaTaxAdditionalSettings = new AVACLOUD_SAND__AdditionalSettings__c();
                
                avaTaxAdditionalSettings.Name=AvaConstants.AVATAX_ADDITIONAL_CUSTOM_SETTING_NAME;
                insert avaTaxAdditionalSettings;
            }            
        }
    }
}