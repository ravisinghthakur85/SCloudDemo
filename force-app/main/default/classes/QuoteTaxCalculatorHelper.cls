public class QuoteTaxCalculatorHelper implements AVA_MAPPER.ICustomBusinessCase	
{
    string accountId = '';
   	string accountName = '';
   	string accountNumber ='';
    decimal shippingCurrency = null;
    string quoteId = '';
    string subsidiaryFieldAPINameLocal = '';
    
    public QuoteTaxCalculatorHelper(string subsidiaryFieldAPIName)	{
        System.debug('In constructor');            
        subsidiaryFieldAPINameLocal = subsidiaryFieldAPIName;
    }

    public void execute(AVA_MAPPER.CreateTransactionModel model, Map <String, List <SObject>> headerResults, Map <String, List <SObject >> lineQueryResult) 
   	{
        try
        {
	        if (headerResults.containsKey('quote')) 
     		{
                List <Quote> quote = headerResults.get('quote');
                
                for (Quote quoteData: quote)
                {
                    //Quote will always be Sales Order 
                    model.Type = AVA_Mapper.DocumentType.SalesInvoice;

                    if(model.addresses == null)
                    {
                        model.addresses = new AVA_Mapper.AddressesModel();
                    }

                    System.debug('subsidiaryFieldAPINameLocal: '+subsidiaryFieldAPINameLocal);
                    if (String.isNotBlank(subsidiaryFieldAPINameLocal) && (subsidiaryFieldAPINameLocal.indexOf('null') == -1) )
                    {
                        string subsidiaryId = string.valueOf(UtilityHelper.getFieldValue(quoteData,subsidiaryFieldAPINameLocal));
                        System.debug('subsidiaryId: '+subsidiaryId);
                        UtilityHelper.setShipFromAddressAndCompany(subsidiaryId,model);
                    }

                    if(quoteData.Account.AVA_MAPPER__Business_Identification_Number__c != null)
                    {
                        model.businessIdentificationNo = quoteData.Account.AVA_MAPPER__Business_Identification_Number__c; 
                    }
                    AVA_MAPPER__Entity_Use_code__c entityuserec = UtilityHelper.fetchEntityUseCode(quoteData.Entity_Use_Code__c);
                    model.customerUsageType = UtilityHelper.trimData(entityuserec.Name, AvaConstants.ENTITY_USE_CODE_LEN);
                    
                    quoteId = quoteData.Id;
                    accountId = quoteData.Account.Id;
                    accountNumber = quoteData.Account.AccountNumber;
                    accountName = UtilityHelper.trimData(quoteData.Account.Name, AvaConstants.ACCOUNT_NAME);
                    
                    shippingCurrency = quoteData.ShippingHandling;
                    
                    model.customerCode = (accountNumber != null) ? String.valueOf(accountNumber) : accountName;
                    
					AVA_MAPPER.AddressLocationInfo shipToAddress = new AVA_MAPPER.AddressLocationInfo();
                    //To check fallback address
                    if(quoteData.ShippingStreet == null && quoteData.ShippingCity == null && quoteData.ShippingState == null && quoteData.ShippingPostalCode == null 
                       && quoteData.ShippingCountry == null)
                    {
                        shipToAddress.line1 = quoteData.BillingStreet;
                        shipToAddress.city = quoteData.BillingCity;
                        shipToAddress.region = quoteData.BillingState;
                        shipToAddress.country = quoteData.BillingCountry;
                        shipToAddress.postalcode = quoteData.BillingPostalCode;
                    }
                    
                    if(model.addresses.shipTo == null && model.addresses.singleLocation == null && model.addresses.pointOfOrderAcceptance == null 
                       && model.addresses.pointOfOrderOrigin == null)
                    {
                    	model.addresses.shipTo = shipToAddress;
                    }
                    
                    if(model.addresses.shipTo != null && model.addresses.shipTo.city == null && model.addresses.shipTo.country == null
                       && model.addresses.shipTo.line1 == null && model.addresses.shipTo.line2 == null && model.addresses.shipTo.line3 == null
                       && model.addresses.shipTo.locationCode == null && model.addresses.shipTo.postalCode == null && model.addresses.shipTo.region == null)
                    {
                    	model.addresses.shipTo = shipToAddress;
                    }

                    if(quoteData.Tax_Date__c != null && quoteData.tax_date__c != quoteData.CreatedDate)
                    {
                        AVA_MAPPER.TaxOverrideModel taxOverride = new AVA_MAPPER.TaxOverrideModel();
                        taxOverride.Reason = AvaConstants.TAX_OVERRIDE_REASON_DATE;
                        taxOverride.type = AVA_MAPPER.TaxOverrideType.TaxDate;	//TaxDate override
                        taxOverride.TaxDate = quoteData.Tax_Date__c;
                        model.taxOverride = taxOverride;
                    }
                    else
                    {
                        model.documentDate = quoteData.CreatedDate;
                    }

                    //Added for VAT implementation
                    model.currencyCode = UserInfo.getDefaultCurrency();
                    if(UserInfo.IsMultiCurrencyOrganization()){
                        if(NULL != Schema.SObjectType.Quote.fields.GetMap().Get('CurrencyIsoCode')){
                            model.currencyCode = String.valueof(quoteData.Get('CurrencyIsoCode'));
                        }
                    }
                }
            }
            
            if (lineQueryResult.containsKey('quotelineitem')) 
            {
               	List <QuoteLineItem> qtLine = lineQueryResult.get('quotelineitem');
                Integer qtLineSize = qtLine.size();

                for (Integer lineCnt=0; lineCnt<qtLineSize; lineCnt++)
               	{
                    model.Lines[lineCnt].Description = UtilityHelper.trimData(qtLine[lineCnt].Pricebookentry.product2.Description, AvaConstants.PRODUCT_DESCRPTION_LEN);
                    if(qtLine[lineCnt].Pricebookentry.ProductCode == null)
                    {
                        model.Lines[lineCnt].ItemCode = UtilityHelper.trimData(qtLine[lineCnt].Pricebookentry.product2.Name, AvaConstants.ITEM_CODE);
                    }
                    else
                    {
                        model.Lines[lineCnt].ItemCode = UtilityHelper.trimData(qtLine[lineCnt].Pricebookentry.ProductCode, AvaConstants.ITEM_CODE);
                    }
                    
                    if(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Enable_UPC_Code_as_ItemCode__c && qtLine[lineCnt].Pricebookentry.product2.UPC__c != null)
                    {
                        model.Lines[lineCnt].ItemCode = UtilityHelper.trimData(('UPC:'+qtLine[lineCnt].Pricebookentry.product2.UPC__c), AvaConstants.ITEM_CODE);
                    }

                    if(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Allow_Tax_Override__c == true && qtLine[lineCnt].AVACLOUD_SAND__Tax_Override__c == true 
                       && qtLine[lineCnt].AVACLOUD_SAND__SalesTax_Line__c != null)
                    {
                        AVA_MAPPER.TaxOverrideModel taxOverrideAmount = new AVA_MAPPER.TaxOverrideModel();
                        taxOverrideAmount.Reason = AvaConstants.TAX_OVERRIDE_REASON_AMOUNT;
                        taxOverrideAmount.type = AVA_MAPPER.TaxOverrideType.TaxAmount;	//TaxDate override
                        taxOverrideAmount.TaxAmount = qtLine[lineCnt].AVACLOUD_SAND__SalesTax_Line__c;
                        model.Lines[lineCnt].taxOverride = taxOverrideAmount;
                    }
                }

                if(shippingCurrency != null)
                {
                    //model.Lines[qtLineSize].Id = qtId;
                    String shippingCode = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).AVACLOUD_SAND__Shipping_Code__c;

                    AVA_MAPPER.LineItemModel lineItem = new AVA_MAPPER.LineItemModel();
                    lineItem.lineNumber = string.valueOf(qtLineSize+1);
                    lineItem.quantity = 1;
                    lineItem.amount = shippingCurrency;
                    lineItem.itemCode = AvaConstants.FREIGHT_ITEM_CODE;
                    lineItem.description = AvaConstants.FREIGHT_DESCRIPTION;

                    lineItem.taxCode = shippingCode;
                    model.lines.add(lineItem);
                }
            }
		}
        catch (Exception e) 
        {
            throw new SalesCloudAvaTaxException('Cause '+e.getCause() +' , '+ 'Message '+e.getMessage() +' , '+ 'Stack Trace '+ e.getStackTraceString(),e);
        }
    }
}