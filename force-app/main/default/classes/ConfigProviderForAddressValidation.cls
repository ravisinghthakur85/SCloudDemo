global class ConfigProviderForAddressValidation  implements AVA_MAPPER.IConfigurationProvider
{
    global AVA_MAPPER.ConfigurationBase hookExtension()
    {   
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        AvaTaxConfigManager config = new AvaTaxConfigManager();
        config.AccountId = Integer.valueOf(avaTaxInstance.Account_ID__c);
        config.LicenseKey = avaTaxInstance.License_Key__c;
        config.environment = AVA_MAPPER.AvaTaxEnvironment.Production;
        if(avaTaxInstance.Sandbox__c) {
            config.environment = AVA_MAPPER.AvaTaxEnvironment.Sandbox;
        }
        config.isAddressValidationEnabled = avaTaxInstance.Validate_Addresses__c;
        return config;
    }
}