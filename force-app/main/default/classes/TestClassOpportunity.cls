@isTest
private class TestClassOpportunity {
    private static AvaTax__c avaTaxInstance = null;
    private static Account testAccount = null;
    private static Contact testContact = null;
    private static Opportunity testOpp = null;
    
	//@testSetup
    private testMethod static void testOpportunity()
    {
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = 'AvaTax Config',
            Account_ID__c = 'test',
            License_Key__c = 'test',
            Company_Code__c = 'default',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = true,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = true,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Validate_Addresses__c = true,
            Allow_Tax_Override__c = false);
        insert avaTaxInstance;
        
        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax');
        insert settings;
            
        Account testAccount = new Account(Name=DateTime.now().format(), BillingStreet = '900 winlsow way e', BillingCity = 'BI', BillingState='WA', BillingPostalCode = '98110', 
                                          BillingCountry = 'US',ShippingStreet = '', ShippingCity = '', ShippingState='', ShippingPostalCode = '', 
                                          ShippingCountry = '',AVA_MAPPER__Business_Identification_Number__c = '1234');
        insert testAccount;
        
        Contact testContact = new Contact(LastName=DateTime.now().format(), AccountId=testAccount.Id,
                                          MailingStreet = '900 winslow way e', MailingCity = 'BI', MailingState='WA', MailingPostalCode = '98110-2450', MailingCountry = 'US',
                                          OtherStreet = '900 winslow way e', OtherCity = 'BI', OtherState='WA', OtherPostalCode = '98110-2450', OtherCountry = 'US');
        
        insert testContact;
        
        Opportunity testOpp = new Opportunity(Name=DateTime.now().format(),StageName='xx',CloseDate=Date.today().addDays(30),AccountId=testAccount.Id, 
                                              AvaTax_Doc_Status__c = null, AvaTax_Message__c = null, Invoice_Message__c = null, 
                                              Sales_Tax__c = null, Tax_Date__c=null);
        insert testOpp;
        
        
        PriceBook2 pricebookTest = new Pricebook2( Name ='Test PriceBook',
                                                  isActive = true                                               
                                                 );
        insert pricebookTest;
        
        Product2 productTest = new Product2( Name ='Test Product',
                                            Family = 'Hardware',
                                            ProductCode = 'testcode',
                                            Description = 'XYZ' 
                                           );
        insert productTest;
        
        Id standardPriceBookId = Test.getStandardPricebookId();
        PriceBookEntry pricebookEntryTest = new PricebookEntry( UnitPrice = 500,
                                                               Pricebook2Id = standardPriceBookId,
                                                               Product2Id = productTest.Id,
                                                               isActive = true                                                
                                                              );
        insert pricebookEntryTest;
        
        testOpp.Sales_Tax__c = 100.00;
        testOpp.tax_date__c = System.today() - 1;
        update testOpp;
                
        OpportunityLineItem testLine = new OpportunityLineItem(Description='xx',OpportunityId=testOpp.Id,
            PricebookEntryId=pricebookEntryTest.Id, Quantity=1.0,TotalPrice=100.00,
            Sales_Tax_Details__c='test tax details', Sales_Tax_Rate__c='10.00%');
            
        insert testLine;

        ConfigurationProvider config = new ConfigurationProvider();
		AVA_MAPPER.TaxCalculator opportunity = new AVA_MAPPER.TaxCalculator('SFConfig','AVACLOUD_SAND',config.hookExtension());
        
        PageReference pageRef = Page.Opportunity_Calculate_Tax;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testOpp.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(testOpp);
        OpportunityTaxCalculator gtOpp = new OpportunityTaxCalculator(sc);
        gtOpp.CalculateTax();

        OpportunityTaxCalculatorHelper gtOpportunityHelper = new OpportunityTaxCalculatorHelper('1234');

        List<ProcessBuilderInput> processBuiilderInput = new List<ProcessBuilderInput>();
        
        ProcessBuilderInput pbInputClass = new ProcessBuilderInput();
        pbInputClass.docId = testOpp.Id;
        pbInputClass.docStatus = 'Temporary';
        processBuiilderInput.add(pbInputClass);

        system.assertEquals(testOpp.AVACLOUD_SAND__AvaTax_Message__c,null);

        OpportunityTaxCalculator.calculateTax(processBuiilderInput);
        OpportunityTaxCalculator oppTaxCalc = new OpportunityTaxCalculator(testOpp.Id);

        system.assertNotEquals(oppTaxCalc, null);
    }

    private testMethod static void testOpportunityMultiEntity()
    {
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = 'AvaTax Config',
            Account_ID__c = 'test',
            License_Key__c = 'test',
            Company_Code__c = 'default',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = true,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = true,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Validate_Addresses__c = true,
            Allow_Tax_Override__c = true);
        insert avaTaxInstance;
        
        Subsidiary__c subsidiary = new Subsidiary__c(Name = 'A1');
        insert subsidiary;
        
        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax', 
                                                                   CustomObjectApiName__c = 'Subsidiary__c',
                                                                  CustomFieldApiName__c = 'Subsidiary_Name__c');
        insert settings;
            
        Account testAccount = new Account(Name=DateTime.now().format(), BillingStreet = '900 winlsow way e', BillingCity = 'BI', BillingState='WA', BillingPostalCode = '98110', 
                                          BillingCountry = 'US',ShippingStreet = '', ShippingCity = '', ShippingState='', ShippingPostalCode = '', 
                                          ShippingCountry = '',AVA_MAPPER__Business_Identification_Number__c = '1234',Subsidiary_Name__c=subsidiary.Id);
        insert testAccount;
        
        Contact testContact = new Contact(LastName=DateTime.now().format(), AccountId=testAccount.Id,
                                          MailingStreet = '900 winslow way e', MailingCity = 'BI', MailingState='WA', MailingPostalCode = '98110-2450', MailingCountry = 'US',
                                          OtherStreet = '900 winslow way e', OtherCity = 'BI', OtherState='WA', OtherPostalCode = '98110-2450', OtherCountry = 'US');
        
        insert testContact;
        
        Opportunity testOpp = new Opportunity(Name=DateTime.now().format(),StageName='xx',CloseDate=Date.today().addDays(30),AccountId=testAccount.Id, 
                                              AvaTax_Doc_Status__c = null, AvaTax_Message__c = null, Invoice_Message__c = null, 
                                              Sales_Tax__c = null, Tax_Date__c=null);
        insert testOpp;
        
        
        PriceBook2 pricebookTest = new Pricebook2( Name ='Test PriceBook',
                                                  isActive = true                                               
                                                 );
        insert pricebookTest;
        
        Product2 productTest = new Product2( Name ='Test Product',
                                            Family = 'Hardware',
                                            Description = 'XYZ',
                                            UPC__c = 'abc'
                                           );
        insert productTest;
        
        Id standardPriceBookId = Test.getStandardPricebookId();
        PriceBookEntry pricebookEntryTest = new PricebookEntry( UnitPrice = 500,
                                                               Pricebook2Id = standardPriceBookId,
                                                               Product2Id = productTest.Id,
                                                               isActive = true                                                
                                                              );
        insert pricebookEntryTest;
        
        testOpp.Sales_Tax__c = 100.00;
        update testOpp;
                
        OpportunityLineItem testLine = new OpportunityLineItem(Description='xx',OpportunityId=testOpp.Id,
            PricebookEntryId=pricebookEntryTest.Id, Quantity=1.0,TotalPrice=100.00);
            
        insert testLine;
		
        OpportunityLineItem testLineToUpdate = [SELECT id, AVACLOUD_SAND__Tax_Override__c, AVACLOUD_SAND__SalesTax_Line__c FROM OpportunityLineItem WHERE id = : testLine.id];
        testLineToUpdate.AVACLOUD_SAND__Tax_Override__c = true;
        testLineToUpdate.AVACLOUD_SAND__SalesTax_Line__c = 10;
        update testLineToUpdate;
            
        ConfigurationProvider config = new ConfigurationProvider();
		AVA_MAPPER.TaxCalculator opportunity = new AVA_MAPPER.TaxCalculator('SFConfig','AVACLOUD_SAND',config.hookExtension());
        
        PageReference pageRef = Page.Opportunity_Calculate_Tax;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testOpp.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(testOpp);
        OpportunityTaxCalculator gtOpp = new OpportunityTaxCalculator(sc);
        gtOpp.CalculateTax();

        OpportunityTaxCalculatorHelper gtOpportunityHelper = new OpportunityTaxCalculatorHelper('1234');

        List<ProcessBuilderInput> processBuiilderInput = new List<ProcessBuilderInput>();
        
        ProcessBuilderInput pbInputClass = new ProcessBuilderInput();
        pbInputClass.docId = testOpp.Id;
        pbInputClass.docStatus = 'Temporary';
        processBuiilderInput.add(pbInputClass);

        system.assertEquals(testOpp.AVACLOUD_SAND__AvaTax_Message__c,null);

        OpportunityTaxCalculator.calculateTax(processBuiilderInput);
        OpportunityTaxCalculator oppTaxCalc = new OpportunityTaxCalculator(testOpp.Id);

        system.assertNotEquals(oppTaxCalc, null);
    }
    
 	private testMethod static void test_addressValidation(){
        AvaTax__c avaTaxInstance = new AvaTax__c(
        Name = 'AvaTax Config',
        Account_ID__c = '1100119845',
        License_Key__c = '55EE9FF205D8224A',
        Company_Code__c = 'default',
		Country__c = 'US',
        City__c = 'Seattle',
        Enable_AvaTax_Tax_Calculation__c = true,
        Enable_UPC_Code_as_ItemCode__c = true,
		Postal_Code__c = '98110',
        Sandbox__c = true,
        Save_transactions_to_AvaTax__c = true,
		State__c = 'WA',
		Street__c = '900 winslow way e',
        Validate_Addresses__c = true);
        insert avaTaxInstance;
        system.assertEquals(avaTaxInstance.State__c,'WA');

        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax');
        insert settings;
        
        ConfigProviderForAddressValidation configObj = new ConfigProviderForAddressValidation();
        configObj.hookExtension();

        system.assertNotEquals(configObj, null);
    }

 	private testMethod static void test_postInstall(){
    	AvaTaxPostInstall postinstall = new AvaTaxPostInstall();
        Test.testInstall(postinstall, null);

        system.assertNotEquals(postinstall, null);
    }
	
    @isTest
    private static void executeMethodExceptionTest(){
		OpportunityTaxCalculatorHelper oppH = new OpportunityTaxCalculatorHelper('Name');
        try{
            oppH.execute(null,null,null);
        }catch(Exception ex){
            system.debug('ex:'+ex.getMessage() + ex.getStackTraceString());
        }
    }
    
    @isTest
    private static void executeMethodTest(){
		OpportunityTaxCalculatorHelper oppH = new OpportunityTaxCalculatorHelper('');
        Map <String, List <SObject>> hResult = new Map <String, List <SObject>>();
        Map <String, List <SObject>> lResult = new Map <String, List <SObject>>();
        
        Account testAccount = new Account(Name=DateTime.now().format(), BillingStreet = '900 winlsow way e', BillingCity = 'BI', BillingState='WA', BillingPostalCode = '98110', 
                                          BillingCountry = 'US',ShippingStreet = '', ShippingCity = '', ShippingState='', ShippingPostalCode = '', 
                                          ShippingCountry = '',AVA_MAPPER__Business_Identification_Number__c = '1234');
        insert testAccount;
        
        Contact testContact = new Contact(LastName=DateTime.now().format(), AccountId=testAccount.Id,
                                          MailingStreet = '900 winslow way e', MailingCity = 'BI', MailingState='WA', MailingPostalCode = '98110-2450', MailingCountry = 'US',
                                          OtherStreet = '900 winslow way e', OtherCity = 'BI', OtherState='WA', OtherPostalCode = '98110-2450', OtherCountry = 'US');
        
        insert testContact;
        
        Opportunity testOpp = new Opportunity(Name=DateTime.now().format(),StageName='xx',CloseDate=Date.today().addDays(30),AccountId=testAccount.Id, 
                                              AvaTax_Doc_Status__c = null, AvaTax_Message__c = null, Invoice_Message__c = null, 
                                              Sales_Tax__c = null, Tax_Date__c=null);
        insert testOpp;
        
        
        PriceBook2 pricebookTest = new Pricebook2( Name ='Test PriceBook',
                                                  isActive = true                                               
                                                 );
        insert pricebookTest;
        
        Product2 productTest = new Product2( Name ='Test Product',
                                            Family = 'Hardware',
                                            Description = 'XYZ',
                                            UPC__c = 'abc'
                                           );
        insert productTest;
        
        Id standardPriceBookId = Test.getStandardPricebookId();
        PriceBookEntry pricebookEntryTest = new PricebookEntry( UnitPrice = 500,
                                                               Pricebook2Id = standardPriceBookId,
                                                               Product2Id = productTest.Id,
                                                               isActive = true                                                
                                                              );
        insert pricebookEntryTest;
        
        OpportunityLineItem testLine = new OpportunityLineItem(Description='xx',OpportunityId=testOpp.Id,
            PricebookEntryId=pricebookEntryTest.Id, Quantity=1.0,TotalPrice=100.00);
            
        insert testLine;
		
        hResult.put('opportunity',new List<SObject>{testOpp});
        lResult.put('opportunitylineitem',new List<SObject>{testLine});
        
        AVA_MAPPER.CreateTransactionModel model = new AVA_MAPPER.CreateTransactionModel();

        try{
        oppH.execute(model,hResult,lResult);
        }
        catch(Exception ex){
			System.debug('In exception');
        }
    }
    
    private testMethod static void testOpportunityDisableTax()
    {
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = 'AvaTax Config',
            Account_ID__c = 'test',
            License_Key__c = 'test',
            Company_Code__c = 'default',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = false,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = true,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Validate_Addresses__c = true,
            Allow_Tax_Override__c = false);
        insert avaTaxInstance;
        
        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax');
        insert settings;
            
        Account testAccount = new Account(Name=DateTime.now().format(), BillingStreet = '900 winlsow way e', BillingCity = 'BI', BillingState='WA', BillingPostalCode = '98110', 
                                          BillingCountry = 'US',ShippingStreet = '', ShippingCity = '', ShippingState='', ShippingPostalCode = '', 
                                          ShippingCountry = '',AVA_MAPPER__Business_Identification_Number__c = '1234');
        insert testAccount;
        
        Contact testContact = new Contact(LastName=DateTime.now().format(), AccountId=testAccount.Id,
                                          MailingStreet = '900 winslow way e', MailingCity = 'BI', MailingState='WA', MailingPostalCode = '98110-2450', MailingCountry = 'US',
                                          OtherStreet = '900 winslow way e', OtherCity = 'BI', OtherState='WA', OtherPostalCode = '98110-2450', OtherCountry = 'US');
        
        insert testContact;
        
        Opportunity testOpp = new Opportunity(Name=DateTime.now().format(),StageName='xx',CloseDate=Date.today().addDays(30),AccountId=testAccount.Id, 
                                              AvaTax_Doc_Status__c = null, AvaTax_Message__c = null, Invoice_Message__c = null, 
                                              Sales_Tax__c = null, Tax_Date__c=null);
        insert testOpp;
        
        
        PriceBook2 pricebookTest = new Pricebook2( Name ='Test PriceBook',
                                                  isActive = true                                               
                                                 );
        insert pricebookTest;
        
        Product2 productTest = new Product2( Name ='Test Product',
                                            Family = 'Hardware',
                                            ProductCode = 'testcode',
                                            Description = 'XYZ' 
                                           );
        insert productTest;
        
        Id standardPriceBookId = Test.getStandardPricebookId();
        PriceBookEntry pricebookEntryTest = new PricebookEntry( UnitPrice = 500,
                                                               Pricebook2Id = standardPriceBookId,
                                                               Product2Id = productTest.Id,
                                                               isActive = true                                                
                                                              );
        insert pricebookEntryTest;
        
        testOpp.Sales_Tax__c = 100.00;
        testOpp.tax_date__c = System.today() - 1;
        update testOpp;
                
        OpportunityLineItem testLine = new OpportunityLineItem(Description='xx',OpportunityId=testOpp.Id,
            PricebookEntryId=pricebookEntryTest.Id, Quantity=1.0,TotalPrice=100.00,
            Sales_Tax_Details__c='test tax details', Sales_Tax_Rate__c='10.00%');
            
        insert testLine;

        ConfigurationProvider config = new ConfigurationProvider();
		AVA_MAPPER.TaxCalculator opportunity = new AVA_MAPPER.TaxCalculator('SFConfig','AVACLOUD_SAND',config.hookExtension());
        
        PageReference pageRef = Page.Opportunity_Calculate_Tax;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testOpp.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(testOpp);
        OpportunityTaxCalculator gtOpp = new OpportunityTaxCalculator(sc);
        gtOpp.CalculateTax();

        OpportunityTaxCalculatorHelper gtOpportunityHelper = new OpportunityTaxCalculatorHelper('1234');

        List<ProcessBuilderInput> processBuiilderInput = new List<ProcessBuilderInput>();
        
        ProcessBuilderInput pbInputClass = new ProcessBuilderInput();
        pbInputClass.docId = testOpp.Id;
        pbInputClass.docStatus = 'Temporary';
        processBuiilderInput.add(pbInputClass);

        system.assertEquals(testOpp.AVACLOUD_SAND__AvaTax_Message__c,null);

        OpportunityTaxCalculator.calculateTax(processBuiilderInput);
        OpportunityTaxCalculator oppTaxCalc = new OpportunityTaxCalculator(testOpp.Id);

        system.assertNotEquals(oppTaxCalc, null);
    }
}