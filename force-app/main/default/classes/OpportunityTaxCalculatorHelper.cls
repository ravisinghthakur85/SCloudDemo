public class OpportunityTaxCalculatorHelper implements AVA_MAPPER.ICustomBusinessCase	
{
    string accountId = '';
    string accountName = '';
    string accountNumber ='';
    decimal shippingCurrency = null;
    string opportunityId = '';
   	string subsidiaryFieldAPINameLocal = '';
     
    public OpportunityTaxCalculatorHelper(string subsidiaryFieldAPIName)	{
        System.debug('In constructor');
            
        subsidiaryFieldAPINameLocal = subsidiaryFieldAPIName;
    }

    public void execute(AVA_MAPPER.CreateTransactionModel model, Map <String, List <SObject>> headerResults, Map <String, List <SObject >> lineQueryResult) 
    {
        try
        {
            if (headerResults.containsKey('opportunity')) 
            {
                List <Opportunity> opportunity = headerResults.get('opportunity');
                
                for (Opportunity opportunityData: opportunity)
                {
                    //Opportunity will always be Sales Order 
                    model.Type = AVA_Mapper.DocumentType.SalesInvoice;
                    
                    if(model.addresses == null)
                    {
                        model.addresses = new AVA_Mapper.AddressesModel();
                    }

                    System.debug('model ship from before address: '+model.addresses.shipFrom);
                    if (String.isNotBlank(subsidiaryFieldAPINameLocal) && (subsidiaryFieldAPINameLocal.indexOf('null') == -1) )
                    {
                        string subsidiaryId = string.valueOf(UtilityHelper.getFieldValue(opportunityData,subsidiaryFieldAPINameLocal));
                        System.debug('subsidiaryId: '+subsidiaryId);
                        UtilityHelper.setShipFromAddressAndCompany(subsidiaryId,model);
                    }

                    System.debug('model ship from after address: '+model.addresses.shipFrom);
                    if(opportunityData.Account.AVA_MAPPER__Business_Identification_Number__c != null)
                    {
                        model.businessIdentificationNo = opportunityData.Account.AVA_MAPPER__Business_Identification_Number__c; 
                    }
                    AVA_MAPPER__Entity_Use_code__c entityuserec = UtilityHelper.fetchEntityUseCode(opportunityData.Entity_Use_Code__c);
                    model.customerUsageType = UtilityHelper.trimData(entityuserec.Name, AvaConstants.ENTITY_USE_CODE_LEN);
                    
                    opportunityId = opportunityData.Id;
                    accountId = opportunityData.Account.Id;
                    accountNumber = opportunityData.Account.AccountNumber;
                    accountName = UtilityHelper.trimData(opportunityData.Account.Name, AvaConstants.ACCOUNT_NAME);
                    
                    if(String.isBlank(model.customerCode))
                    {
	                    model.customerCode = (accountNumber != null) ? String.valueOf(accountNumber) : accountName;
                    }
                    
					AVA_MAPPER.AddressLocationInfo shipToAddress = new AVA_MAPPER.AddressLocationInfo();
                    //To check fallback address
                    if(opportunityData.Account.ShippingStreet == null && opportunityData.Account.ShippingCity == null && opportunityData.Account.ShippingState == null 
                       && opportunityData.Account.ShippingPostalCode == null && opportunityData.Account.ShippingCountry == null)
                    {
                        shiptoAddress.line1 = opportunityData.Account.BillingStreet;
                        shiptoAddress.city = opportunityData.Account.BillingCity;
                        shiptoAddress.region = opportunityData.Account.BillingState;
                        shiptoAddress.country = opportunityData.Account.BillingCountry;
                        shiptoAddress.postalcode = opportunityData.Account.BillingPostalCode;
                        
                        System.debug('In Opp Helper billing address');
                    }
                    
                    if(model.addresses.shipTo == null && model.addresses.singleLocation == null && model.addresses.pointOfOrderAcceptance == null 
                       && model.addresses.pointOfOrderOrigin == null)
                    {
                    	model.addresses.shipTo = shipToAddress;
                        System.debug('In shipTo == null');
                    }
                    
                    if(model.addresses.shipTo != null && model.addresses.shipTo.city == null && model.addresses.shipTo.country == null
                       && model.addresses.shipTo.line1 == null && model.addresses.shipTo.line2 == null && model.addresses.shipTo.line3 == null
                       && model.addresses.shipTo.locationCode == null && model.addresses.shipTo.postalCode == null && model.addresses.shipTo.region == null)
                    {
                    	model.addresses.shipTo = shipToAddress;
                       	System.debug('In shipTo != null');
                     }

                    if(opportunityData.Tax_Date__c != null && opportunityData.tax_date__c != opportunityData.CreatedDate)
                    {
                        AVA_MAPPER.TaxOverrideModel taxOverride = new AVA_MAPPER.TaxOverrideModel();
                        taxOverride.Reason = AvaConstants.TAX_OVERRIDE_REASON_DATE;
                        taxOverride.type = AVA_MAPPER.TaxOverrideType.TaxDate;	//TaxDate override
                        taxOverride.TaxDate = opportunityData.Tax_Date__c;
                        model.taxOverride = taxOverride;
                    }
                    else
                    {
                        model.documentDate = opportunityData.CreatedDate;
                    }
                    
                    //Added for VAT implementation
                    model.currencyCode = UserInfo.getDefaultCurrency();
                    if(UserInfo.IsMultiCurrencyOrganization()){
                        if(NULL != Schema.SObjectType.Opportunity.fields.GetMap().Get('CurrencyIsoCode')){
                            model.currencyCode = String.valueof(opportunityData.Get('CurrencyIsoCode'));
                        }
                    }
                }
            }
            
            if (lineQueryResult.containsKey('opportunitylineitem')) 
            {
                List <OpportunityLineItem> opptyLine = lineQueryResult.get('opportunitylineitem');
                Integer opptyLineSize = opptyLine.size();
                
                for (Integer lineCnt=0; lineCnt<opptyLineSize; lineCnt++)
                {
                    model.Lines[lineCnt].Description = UtilityHelper.trimData(opptyLine[lineCnt].Pricebookentry.product2.Description, AvaConstants.PRODUCT_DESCRPTION_LEN);
                    if(opptyLine[lineCnt].Pricebookentry.ProductCode == null)
                    {
                        model.Lines[lineCnt].ItemCode = UtilityHelper.trimData(opptyLine[lineCnt].Pricebookentry.product2.Name, AvaConstants.ITEM_CODE);
                    }
                    else
                    {
                        model.Lines[lineCnt].ItemCode = UtilityHelper.trimData(opptyLine[lineCnt].Pricebookentry.ProductCode, AvaConstants.ITEM_CODE);
                    }
                    
                    if(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Enable_UPC_Code_as_ItemCode__c && opptyLine[lineCnt].Pricebookentry.product2.UPC__c != null)
                    {
                        model.Lines[lineCnt].ItemCode = UtilityHelper.trimData(('UPC:'+opptyLine[lineCnt].Pricebookentry.product2.UPC__c), AvaConstants.ITEM_CODE);
                    }
                    system.debug('AVACLOUD_SAND__Allow_Tax_Override__c:'+AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).AVACLOUD_SAND__Allow_Tax_Override__c);
                    system.debug('opptyLine[lineCnt].AVACLOUD_SAND__Tax_Override__c:'+opptyLine[lineCnt].AVACLOUD_SAND__Tax_Override__c);
                    system.debug('opptyLine[lineCnt].AVACLOUD_SAND__SalesTax_Line__c:'+opptyLine[lineCnt].AVACLOUD_SAND__SalesTax_Line__c);
                    if(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).AVACLOUD_SAND__Allow_Tax_Override__c == true && opptyLine[lineCnt].AVACLOUD_SAND__Tax_Override__c == true 
                       && opptyLine[lineCnt].AVACLOUD_SAND__SalesTax_Line__c != null)
                    {
                        AVA_MAPPER.TaxOverrideModel taxOverrideAmount = new AVA_MAPPER.TaxOverrideModel();
                        taxOverrideAmount.Reason = AvaConstants.TAX_OVERRIDE_REASON_AMOUNT;
                        taxOverrideAmount.type = AVA_MAPPER.TaxOverrideType.TaxAmount;	//TaxDate override
                        taxOverrideAmount.TaxAmount = opptyLine[lineCnt].AVACLOUD_SAND__SalesTax_Line__c;
                        model.Lines[lineCnt].taxOverride = taxOverrideAmount;
                    }
                }
            }
        }
        catch (Exception e) 
        {
            throw new SalesCloudAvaTaxException('Cause '+e.getCause() +' , '+ 'Message '+e.getMessage() +' , '+ 'Stack Trace '+ e.getStackTraceString(),e);
        }
    }
}