trigger avaCancelTaxTrigger on Order (before delete) {
	OrderTaxCalculator.cancelTaxList(UtilityHelper.listGetOrderId(Trigger.old));
}